#include < ros/ros.h>
#include <ros/ros.h>
#include <nav_msgs/MapMetaData.h>
#include <nav_msgs/OccupancyGrid.h>
#include "std_msgs/Header.h"
#include "nav_msgs/MapMetaData.h"
#include <iostream.h>
#include <cstdlib> 
#include <limits.h> 
#include <stdio.h> 
  
// Number of vertices in the graph 
#define V 1000


//int *p=NULL,*q=NULL;


int minDistance(int dist[], bool sptSet[]) 
{ 
    // Initialize min value 
    int min = INT_MAX, min_index; 
  
    for (int v = 0; v < V; v++) 
        if (sptSet[v] == false && dist[v] <= min) 
            min = dist[v], min_index = v; 
  
    return min_index; 
} 

/*int***/void get2DmapDjikstra( const nav_msgs::OccupancyGrid& msg)
{
  
  std_msgs::Header header = msg->header;
  nav_msgs::MapMetaData info = msg->info;
  nav_msgs::OccupancyGrid data = msg->data;
  ROS_INFO("Got map %d %d", info.width, info.height);
  int**  a= (int **)calloc( info.width,sizeof(int *)); //a[info.width][info.height];
   
   for (unsigned int x = 0; x < info.width; x++)//rowize iteration
{
    a[x] = (int *)calloc(info.height,sizeof(int));
    for (unsigned int y = 0; y < info.width; y++)
        {
           
           a[x][y]=msg->data[x+ info.width * y]
        }
}


    int A[1000][1000], m=info.width, n=info.height, i, j;
    //p=&m;
    //q=&n;

  //  cout << "Transpose of Matrix : \n ";

    for (i = 0; i < n; i++)

    {

        for (j = 0; j < m; j++)

             A[i][j]=a[j][i] ;

    }
//############## DJIKSTRA
    int **graph = A; int src =0;

  
  int dist[V]; // The output array.  dist[i] will hold the shortest 
    // distance from src to i 
  
    bool sptSet[V]; // sptSet[i] will be true if vertex i is included in shortest 
    // path tree or shortest distance from src to i is finalized 
  
    // Initialize all distances as INFINITE and stpSet[] as false 
    for (int i = 0; i < V; i++) 
        dist[i] = INT_MAX, sptSet[i] = false; 
  
    // Distance of source vertex from itself is always 0 
    dist[src] = 0; 
  
    // Find shortest path for all vertices 
    for (int count = 0; count < V - 1; count++) { 
        // Pick the minimum distance vertex from the set of vertices not 
        // yet processed. u is always equal to src in the first iteration. 
        int u = minDistance(dist, sptSet); 
  
        // Mark the picked vertex as processed 
        sptSet[u] = true; 
  
        // Update dist value of the adjacent vertices of the picked vertex. 
        for (int v = 0; v < V; v++) 
  
            // Update dist[v] only if is not in sptSet, there is an edge from 
            // u to v, and total weight of path from src to  v through u is 
            // smaller than current value of dist[v] 
            if (!sptSet[v] && graph[u][v] && dist[u] != INT_MAX 
                && dist[u] + graph[u][v] < dist[v]) 
                dist[v] = dist[u] + graph[u][v]; 


}


    int main(int argc, char **argv) {

    ros::init(argc, argv, "shortestpath");

    ros::Nodehandle n("~");

    ros::Subscriber sub = n.subscribe("/map", 1000, get2DmapDjikstra);



    }


