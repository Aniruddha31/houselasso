//#include "opencv3/core/core.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

#include <opencv2/imgproc/imgproc.hpp>//a

#include "ros/ros.h"           //a
#include "std_msgs/String.h"//a
#include <sstream>//a

#include <image_transport/image_transport.h>//a
#include <sensor_msgs/image_encodings.h>//a
#include <cv_bridge/cv_bridge.h>//a
//#include <opencv/opencv.hpp>//a

namespace enc = sensor_msgs::image_encodings;//a

int main( int argc, char** argv )
{

    


   cv::Mat image_gray = cv::imread("basic_map.png", CV_LOAD_IMAGE_GRAYSCALE);

   /* if(! image_gray.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }*/



   // convert gray image to binary image 
   // After threshold, all values are either (0 or 100)
   cv::Mat imgage_bw;
   cv::threshold(image_gray, imgage_bw, 50, 100, cv::THRESH_BINARY);

   // 0 for blocked cell and 1 for free cell
   //0 for black , 1 for white
   cv::Mat image_grid = imgage_bw/100;  

#if 1
   
    ros::init(argc, argv, "loadImg2");//a
    ros::NodeHandle n;
    image_transport::ImageTransport it_(n);
    image_transport::Publisher image_pub_ = it_.advertise("/occupancy_grid", 1);

    cv_bridge::CvImagePtr cv_ptr(new cv_bridge::CvImage);


    ros::Rate loop_rate(10); //10 hz
    while(ros::ok()){


        #if 1
            ros::Time time = ros::Time::now();
            cv_ptr->encoding = "bgr8";
            cv_ptr->header.stamp = time;
            cv_ptr->header.frame_id = "/occupancy_grid";
        #endif

            cv_ptr->image = image_grid;
            image_pub_.publish(cv_ptr->toImageMsg());

            ROS_INFO("ImageMsg Sent.");
            ROS_INFO("Subscribers: %d", image_pub_.getNumSubscribers());


        ros::spinOnce();
        loop_rate.sleep();
    }
#endif

    ros::spin();


    return 0;
}