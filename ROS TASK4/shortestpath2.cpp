

#include <ros/ros.h>
#include <nav_msgs/MapMetaData.h>
#include <nav_msgs/OccupancyGrid.h>
#include "std_msgs/Header.h"
#include "nav_msgs/MapMetaData.h"
#include <iostream>
#include <cstdlib> 
#include <limits.h> 
#include <stdio.h> 

#include "geometry_msgs/Point.h"
#include "beginner_tutorials/my_msg.h"  

#include <vector>


 
struct Point {
    float x;
    float y;
};
  
// Number of vertices in the graph 
#define V 1000


//int *p=NULL,*q=NULL;

// A utility function to find the  
// vertex with minimum distance 
// value, from the set of vertices 
// not yet included in shortest 
// path tree 
int minDistance(int dist[],  
                bool sptSet[]) 
{ 
      
    // Initialize min value 
    int min = INT_MAX, min_index; 
  
    for (int v = 0; v < V; v++) 
        if (sptSet[v] == false && 
                   dist[v] <= min) 
            min = dist[v], min_index = v; 
  
    return min_index; 
} 
  
// Function to print shortest 
// path from source to j 
// using parent array 
void printPath(int parent[], int j) 
{ 
      
    // Base Case : If j is source 
    if (parent[j] == - 1) 
        return; 
  
    printPath(parent, parent[j]); 
  
   // printf("%d ", j); 
} 
  
// A utility function to print  
// the constructed distance 
// array 
int printSolution(int dist[], int n,  //change return type
                      int parent[]) 
{ 
    int src = 0; 
   // printf("Vertex\t Distance\tPath"); 

  // creating the vector    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  Point my_array[1000];
  Point point;
  for (int i=0; i < 11; i++) {    // modify !!!!!!!!!!!!!!!
    point.x = i;                 // point.x = (parent%1000)
    point.y = i;                 //point.y = parent - (parent%1000)*1000
    my_array[i] = point;
  }

std::vector<Point> my_vector (my_array, my_array + sizeof(my_array) / sizeof(Point));

 // the message to be published
  beginner_tutorials::my_msg msg;
  //msg.another_field = ;

// loop control
  int count = 0;
  while (ros::ok())
  {
    msg.points.clear();
 
    msg.another_field = count;
    int i =0 ;
    for (std::vector<Point>::iterator it = my_vector.begin(); it != my_vector.end(); ++it) {
        geometry_msgs::Point point;
        point.x = (*it).x;
        point.y = (*it).y;
        point.z = 0;
        msg.points.push_back(point);
        i++;
    }
 /*   for (int i = 1; i < V; i++) 
    { 
        printf("\n%d -> %d \t\t %d\t\t%d ", 
                      src, i, dist[i], src); 
        printPath(parent, i); 
    } */
}
} 

/*int***/void get2DmapDjikstra( const nav_msgs::OccupancyGrid::ConstPtr& msg)
{
  
  //std_msgs::Header header = msg->header;
  nav_msgs::MapMetaData info = msg->info;
  nav_msgs::OccupancyGrid data = msg->data;
  ROS_INFO("Got map %d %d", info.width, info.height);
  int**  a= (int **)calloc( info.width,sizeof(int *)); //a[info.width][info.height];
   
   for (unsigned int x = 0; x < info.width; x++)//rowize iteration
{
    a[x] = (int *)calloc(info.height,sizeof(int));
    for (unsigned int y = 0; y < info.width; y++)
        {
           
           a[x][y]=msg->data[x+ info.width * y];
        }
}


    int A[1000][1000], m=info.width, n=info.height, i, j;
    //p=&m;
    //q=&n;

  //  cout << "Transpose of Matrix : \n ";

    for (i = 0; i < n; i++)

    {

        for (j = 0; j < m; j++)

             A[i][j]=a[j][i] ;

    }
//############## DJIKSTRA
    /*int *graph = A;*/ int src =0;

  // The output array. dist[i] 
    // will hold the shortest 
    // distance from src to i 
    int dist[V];  
  
    // sptSet[i] will true if vertex 
    // i is included / in shortest 
    // path tree or shortest distance  
    // from src to i is finalized 
    bool sptSet[V]; 
  
    // Parent array to store 
    // shortest path tree 
    int parent[V]; 
  
    // Initialize all distances as  
    // INFINITE and stpSet[] as false 
    for (int i = 0; i < V; i++) 
    { 
        parent[0] = -1; 
        dist[i] = INT_MAX; 
        sptSet[i] = false; 
    } 
  
    // Distance of source vertex  
    // from itself is always 0 
    dist[src] = 0; 
  
    // Find shortest path 
    // for all vertices 
    for (int count = 0; count < V - 1; count++) 
    { 
        // Pick the minimum distance 
        // vertex from the set of 
        // vertices not yet processed.  
        // u is always equal to src 
        // in first iteration. 
        int u = minDistance(dist, sptSet); 
  
        // Mark the picked vertex  
        // as processed 
        sptSet[u] = true; 
  
        // Update dist value of the  
        // adjacent vertices of the 
        // picked vertex. 
        for (int v = 0; v < V; v++) 
  
            // Update dist[v] only if is 
            // not in sptSet, there is 
            // an edge from u to v, and  
            // total weight of path from 
            // src to v through u is smaller 
            // than current value of 
            // dist[v] 
            if (!sptSet[v] && A[u][v] && 
                dist[u] + A[u][v] < dist[v]) 
            { 
                parent[v] = u; 
                dist[v] = dist[u] + A[u][v]; 
            }  
    } 
  
    // print the constructed 
    // distance array 
    printSolution(dist, V, parent); 

    

}


    int main(int argc, char **argv) {

    ros::init(argc, argv, "shortestpath2");

    ros::NodeHandle n("~");

    ros::Subscriber sub = n.subscribe("/map", 1000, get2DmapDjikstra);
    ros::Publisher pub = n.advertise<beginner_tutorials::my_msg>("my_topic", 1);
    ros::Rate loop_rate(0.5);


    }


/*will work only for 1000x1000 png image
starting vertex is fixed that is node 0*/
