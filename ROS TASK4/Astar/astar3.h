
#include <iostream>
#include <opencv2/opencv.hpp>
//
#include <iostream>
#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>  //Receive the map via this topic.  Publish the inflation map(mask) via this topic.
//#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>   // to Receive the start point 
#include <geometry_msgs/PoseStamped.h>   //to Receive the target point
#include <geometry_msgs/Quaternion.h>
//#include <Vector3.h>
#include <tf/tf.h>
#include <opencv2/opencv.hpp>


//

using namespace std;
using namespace cv;


namespace pathplanning{

enum NodeType{
    obstacle = 0,
    free,
    inOpenList,
    inCloseList       //The CLOSED list is initially empty but will grow to contain those nodes that have already been examined and are of no further interest to the search.
};

struct Node{
    Point point;  // node coordinate
    double F,G,H;     // heuristic , g, f =g+h
    Node* parent; // parent node

    Node(Point _point = Point(0, 0)):point(_point), F(0), G(0), H(0), parent(NULL) // this is an example of a constructor inside struct i.e. <struct_name>() : initializer list{} ,  initializer list i.e. <field_name>(<value to initialize>)  also "()" these brackets contain values on which field variables depend
    {
    }
};

struct AstarConfig{   
    int OccupyThresh;       // 0~255

    AstarConfig( int _OccupyThresh = -1): OccupyThresh(_OccupyThresh)
    {
    }
};

class Astar{

public:
    // Interface function
    void InitAstar(Mat& _Map, AstarConfig _config = AstarConfig());
    void InitAstar(Mat& _Map, Mat& Mask, AstarConfig _config = AstarConfig());
    void PathPlanning(Point _startPoint, Point _targetPoint, vector<Point>& path);
    void DrawPath(Mat& _Map, vector<Point>& path, InputArray Mask = noArray(), Scalar color = Scalar(0, 0, 255),   // InputArray => it is opencv2 class , noArray() simply returns an empty array  //Scalar in opencv determines RGB values
            int thickness = 1, Scalar maskcolor = Scalar(255, 255, 255));    // 255 means white pixel

private:
    void MapProcess(Mat& Mask);
    Node* FindPath();
    void GetPath(Node* TailNode, vector<Point>& path);
    double calcEuclidean(int col, int row);
    double calcManhattan(int col, int row);
    bool isValid(int y,int x);

private:
    //Object
    Mat Map;
    Point startPoint, targetPoint;
    
    Mat LabelMap;
    AstarConfig config;
    vector<Node*> OpenList;  // open list  => The OPEN list is like a shopping list of nodes that are candidates for examination. Initially it contains just one node, the start node.
    vector<Node*> PathList;  // path list
};

}


