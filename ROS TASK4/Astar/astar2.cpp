
#include "astar.h"

namespace pathplanning{

void Astar::InitAstar(Mat& _Map, AstarConfig _config)
{
    Mat Mask;
    InitAstar(_Map, Mask, _config);
}

void Astar::InitAstar(Mat& _Map, Mat& Mask, AstarConfig _config)
{
    char neighbor8[8][2] = {
            {-1, -1}, {-1, 0}, {-1, 1},
            {0, -1},            {0, 1},
            {1, -1},   {1, 0},  {1, 1}
    };

    Map = _Map;
    config = _config;
    if(true)//config.Diagonal
    {
        neighbor = Mat(8, 2, CV_8S, neighbor8).clone();
    }
    

    MapProcess(Mask);
}

void Astar::PathPlanning(Point _startPoint, Point _targetPoint, vector<Point>& path)
{
    // Get variables
    startPoint = _startPoint;
    targetPoint = _targetPoint;

    // Path Planning
    Node* TailNode = FindPath();
    GetPath(TailNode, path);
}

void Astar::DrawPath(Mat& _Map, vector<Point>& path, InputArray Mask, Scalar color,
        int thickness, Scalar maskcolor)
{
    if(path.empty())
    {
        cout << "Path is empty!" << endl;
        return;
    }
    _Map.setTo(maskcolor, Mask);
    for(auto it:path)
    {
        rectangle(_Map, it, it, color, thickness);
    }
}

void Astar::MapProcess(Mat& Mask)
{
    int width = Map.cols;
    int height = Map.rows;
    Mat _Map = Map.clone();  //Mat B = A.clone();  // B is a deep copy of A. (B has its own copy of the pixels)

    // Transform RGB to gray image
    if(_Map.channels() == 3)
    {
        cvtColor(_Map.clone(), _Map, CV_BGR2GRAY);  ///// Convert the image to Gray cvtColor( src, src_gray, CV_BGR2GRAY );
    }

    // Binarize
    if(config.OccupyThresh < 0)
    {
        threshold(_Map.clone(), _Map, 0, 255, CV_THRESH_OTSU);
    } else
    {
        threshold(_Map.clone(), _Map, config.OccupyThresh, 255, CV_THRESH_BINARY);  // threshold( src_gray, dst, threshold_value, max_BINARY_value,threshold_type );
    }

    // note we are just updating _Map

    /*
    As you can see, the function threshold is invoked. We give 5 parameters:

    src_gray: Our input image
    dst: Destination (output) image
    threshold_value: The thresh value with respect to which the thresholding operation is made
    max_BINARY_value: The value used with the Binary thresholding operations (to set the chosen pixels)
    threshold_type: One of the 5 thresholding operations. They are listed in the comment section of the function above.

    */

    //  !!!
    Mat src = _Map.clone();
    // Get mask
    bitwise_xor(src, _Map, Mask);
    //!!!!!!!!!!!!!!!

    // Initial LabelMap  which is private object of type cv::Mat in dijkstra class in mydijkstra.h
    // it keeps values defined in the "enum"
    LabelMap = Mat::zeros(height, width, CV_8UC1);
    for(int y=0;y<height;y++)
    {
        for(int x=0;x<width;x++)
        {
            if(_Map.at<uchar>(y, x) == 0)  // _Map is used here !!!!!!!!!!!
            {
                LabelMap.at<uchar>(y, x) = obstacle;
            }
            else
            {
                LabelMap.at<uchar>(y, x) = free;
            }
        }
    }
}// MapProcess ends

// A Utility Function to calculate the 'h' heuristics. 
double Astar::calculateHValue(int y, int x) 
{ 
    // Return using the distance formula 
    return ((double) max (abs(targetPoint.x - x) ,abs(targetPoint.y - y))); 
} 

double Astar::calcManhattan(int y, int x)
{
    return ((double)  (abs(targetPoint.x - x) + abs(targetPoint.y - y)));
}

bool Astar::isValid(int y, int x)
{
	int width = Map.cols;
    int height = Map.rows;
	return (!(x < 0 || x >= width || y < 0 || y >= height));
}

Node* Astar::FindPath()
{
    int width = Map.cols;
    int height = Map.rows;
    Mat _LabelMap = LabelMap.clone();  

    // Add startPoint to (OpenList  i.e. of type  vector<Node*>)
    OpenList.clear();                         
    OpenList.push_back(new Node(startPoint));
    _LabelMap.at<uchar>(startPoint.y, startPoint.x) = inOpenList;

    while(!OpenList.empty())
    {
        // Find the node with least F value  , F = g + heuristicFunct in astar and F = g in dijkstra
        Node* CurNode = OpenList[0];     // take openlist values only
        int index = 0;
        int length = OpenList.size();
        for(int i = 0;i < length;i++)              //"do s current = argmin f {OPEN}""""""
        {
            if(OpenList[i]->f < CurNode->f)
            {
                CurNode = OpenList[i];
                index = i;
            }
        }
        int curX = CurNode->point.x;
        int curY = CurNode->point.y;
        OpenList.erase(OpenList.begin() + index);       // Delete CurNode from OpenList and put in closedList
        _LabelMap.at<uchar>(curY, curX) = inCloseList;

        // Determine whether arrive the target point
        if(curX == targetPoint.x && curY == targetPoint.y)
        {
            return CurNode; // Find a valid path
        }

        // Traverse the neighborhood
/*        for(int k = 0;k < neighbor.rows;k++)      // for s' ∈ Succ(s current )
        {
            int y = curY + neighbor.at<char>(k, 0);
            
            

            int x = curX + neighbor.at<char>(k, 1);

            //  ROS_INFO("neighbor.at<char>(k, 0) /  y / curY : %d %d %d %d", neighbor.at<char>(k, 0), y, curY, k);


            if(x < 0 || x >= width || y < 0 || y >= height)   // check for boundary conditions in occupancy grid and change neighbour if true
            {
                continue;
            }
            if(_LabelMap.at<uchar>(y, x) == free || _LabelMap.at<uchar>(y, x) == inOpenList)   // check if (y,x) is inopenlist
            {
                if(config.Diagonal)
                {
                    // Determine whether a diagonal line can pass
                    bool walkable = true;
                    if(y == curY - 1 && _LabelMap.at<uchar>(curY - 1, curX) == obstacle)
                    {
                        walkable = false;
                    }
                    else if(y == curY + 1 && _LabelMap.at<uchar>(curY + 1, curX) == obstacle)
                    {
                        walkable = false;
                    }

                    if(x == curX - 1 && _LabelMap.at<uchar>(curY, curX - 1) == obstacle)
                    {
                        walkable = false;
                    }
                    else if(x == curX + 1 && _LabelMap.at<uchar>(curY, curX + 1) == obstacle)
                    {
                        walkable = false;
                    }
                    if(!walkable)
                    {
                        continue;
                    }
                }

                // Calculate distance
                double addist, f,g,h;
                if(abs(x - curX) == 1 && abs(y - curY) == 1)
                {
                    addist = 14 + calculateHValue(y, x);
                }
                else
                {
                    addist = 10 + calculateHValue(y, x);
                }
                f = CurNode->f + addist;

                // Update the f value of node
                if(_LabelMap.at<uchar>(y, x) == free)
                {
                    Node* node = new Node();
                    node->point = Point(x, y);
                    node->parent = CurNode;
                    node->f = f;
                    OpenList.push_back(node);
                    _LabelMap.at<uchar>(y, x) = inOpenList;
                }
                else // _LabelMap.at<uchar>(y, x) == inOpenList
                {
                    // Find the node
                    Node* node = NULL;
                    int length = OpenList.size();
                    for(int i = 0;i < length;i++)
                    {
                        if(OpenList[i]->point.x ==  x && OpenList[i]->point.y ==  y)
                        {
                            node = OpenList[i];
                            break;
                        }
                    }
                    if(f < node->f)
                    {
                        node->f = f;
                        node->parent = CurNode;
                    }
                }
            }
        }*/


        /* 
		Generating all the 8 successor of this cell 

			N.W N N.E 
			\ | / 
			\ | / 
			W----Cell----E 
				/ | \ 
			/ | \ 
			S.W S S.E 

		Cell-->Popped Cell (i, j) 
		N --> North	 (i-1, j) 
		S --> South	 (i+1, j) 
		E --> East	 (i, j+1) 
		W --> West		 (i, j-1) 
		N.E--> North-East (i-1, j+1) 
		N.W--> North-West (i-1, j-1) 
		S.E--> South-East (i+1, j+1) 
		S.W--> South-West (i+1, j-1)*/

		// To store the 'g', 'h' and 'f' of the 8 successors 
		double gNew, hNew, fNew; 

		//----------- 1st Successor (North) ------------ 

		// Only process this cell if this is a valid one 
		if (isValid(curY, curX-1) == true)
		{
			int y = curY;
			int x = curX-1;
			if(_LabelMap.at<uchar>(y, x) == free || _LabelMap.at<uchar>(y, x) == inOpenList)   // check if (y,x) is inopenlist
			{
				// Calculate distance
                double addist, f,g,h;
                if(abs(x - curX) == 1 && abs(y - curY) == 1)
                {
                    addist = 14 + calculateHValue(y, x);
                }
                else
                {
                    addist = 10 + calculateHValue(y, x);
                }
                f = CurNode->f + addist;

                // Update the f value of node
                if(_LabelMap.at<uchar>(y, x) == free)
                {
                    Node* node = new Node();
                    node->point = Point(x, y);
                    node->parent = CurNode;
                    node->f = f;
                    OpenList.push_back(node);
                    _LabelMap.at<uchar>(y, x) = inOpenList;
                }
                else // _LabelMap.at<uchar>(y, x) == inOpenList
                {
                    // Find the node
                    Node* node = NULL;
                    int length = OpenList.size();
                    for(int i = 0;i < length;i++)
                    {
                        if(OpenList[i]->point.x ==  x && OpenList[i]->point.y ==  y)
                        {
                            node = OpenList[i];
                            break;
                        }
                    }
                    if(f < node->f)
                    {
                        node->f = f;
                        node->parent = CurNode;
                    }
                }

			}

		}

		//----------- 2nd Successor (South) ------------ 
  
        // Only process this cell if this is a valid one 
        if (isValid(curY, curX +1) == true) 
        {
        	int y = curY;
        	int x = curX +1;
        	if(_LabelMap.at<uchar>(y, x) == free || _LabelMap.at<uchar>(y, x) == inOpenList)   // check if (y,x) is inopenlist
        	{
        		// Calculate distance
                double addist, f,g,h;
                if(abs(x - curX) == 1 && abs(y - curY) == 1)
                {
                    addist = 14 + calculateHValue(y, x);
                }
                else
                {
                    addist = 10 + calculateHValue(y, x);
                }
                f = CurNode->f + addist;

                // Update the f value of node
                if(_LabelMap.at<uchar>(y, x) == free)
                {
                    Node* node = new Node();
                    node->point = Point(x, y);
                    node->parent = CurNode;
                    node->f = f;
                    OpenList.push_back(node);
                    _LabelMap.at<uchar>(y, x) = inOpenList;
                }
                else // _LabelMap.at<uchar>(y, x) == inOpenList
                {
                    // Find the node
                    Node* node = NULL;
                    int length = OpenList.size();
                    for(int i = 0;i < length;i++)
                    {
                        if(OpenList[i]->point.x ==  x && OpenList[i]->point.y ==  y)
                        {
                            node = OpenList[i];
                            break;
                        }
                    }
                    if(f < node->f)
                    {
                        node->f = f;
                        node->parent = CurNode;
                    }
                }

        	}

        }

        //----------- 3rd Successor (East) ------------ 
  
        // Only process this cell if this is a valid one 
        if (isValid (curY +1, curX) == true) 
        {
        	int y = curY +1;
        	int x = curX;
        	if(_LabelMap.at<uchar>(y, x) == free || _LabelMap.at<uchar>(y, x) == inOpenList)   // check if (y,x) is inopenlist
        	{
        		// Calculate distance
                double addist, f,g,h;
                if(abs(x - curX) == 1 && abs(y - curY) == 1)
                {
                    addist = 14 + calculateHValue(y, x);
                }
                else
                {
                    addist = 10 + calculateHValue(y, x);
                }
                f = CurNode->f + addist;

                // Update the f value of node
                if(_LabelMap.at<uchar>(y, x) == free)
                {
                    Node* node = new Node();
                    node->point = Point(x, y);
                    node->parent = CurNode;
                    node->f = f;
                    OpenList.push_back(node);
                    _LabelMap.at<uchar>(y, x) = inOpenList;
                }
                else // _LabelMap.at<uchar>(y, x) == inOpenList
                {
                    // Find the node
                    Node* node = NULL;
                    int length = OpenList.size();
                    for(int i = 0;i < length;i++)
                    {
                        if(OpenList[i]->point.x ==  x && OpenList[i]->point.y ==  y)
                        {
                            node = OpenList[i];
                            break;
                        }
                    }
                    if(f < node->f)
                    {
                        node->f = f;
                        node->parent = CurNode;
                    }
                }

        	}

        }

         //----------- 4th Successor (West) ------------ 
  
        // Only process this cell if this is a valid one 
        if (isValid(curY-1, curX) == true) 
        {
        	int y = curY-1;
        	int x = curX;
        	if(_LabelMap.at<uchar>(y, x) == free || _LabelMap.at<uchar>(y, x) == inOpenList)   // check if (y,x) is inopenlist
        	{
        		// Calculate distance
                double addist, f,g,h;
                if(abs(x - curX) == 1 && abs(y - curY) == 1)
                {
                    addist = 14 + calculateHValue(y, x);
                }
                else
                {
                    addist = 10 + calculateHValue(y, x);
                }
                f = CurNode->f + addist;

                // Update the f value of node
                if(_LabelMap.at<uchar>(y, x) == free)
                {
                    Node* node = new Node();
                    node->point = Point(x, y);
                    node->parent = CurNode;
                    node->f = f;
                    OpenList.push_back(node);
                    _LabelMap.at<uchar>(y, x) = inOpenList;
                }
                else // _LabelMap.at<uchar>(y, x) == inOpenList
                {
                    // Find the node
                    Node* node = NULL;
                    int length = OpenList.size();
                    for(int i = 0;i < length;i++)
                    {
                        if(OpenList[i]->point.x ==  x && OpenList[i]->point.y ==  y)
                        {
                            node = OpenList[i];
                            break;
                        }
                    }
                    if(f < node->f)
                    {
                        node->f = f;
                        node->parent = CurNode;
                    }
                }

        	}

        }

        //----------- 5th Successor (North-East) ------------ 
  
        // Only process this cell if this is a valid one 
        if (isValid(curY+1, curX-1) == true) 
        {
        	int y = curY+1;
        	int x = curX-1;
        	if(_LabelMap.at<uchar>(y, x) == free || _LabelMap.at<uchar>(y, x) == inOpenList)   // check if (y,x) is inopenlist
        	{
        		// Calculate distance
                double addist, f,g,h;
                if(abs(x - curX) == 1 && abs(y - curY) == 1)
                {
                    addist = 14 + calculateHValue(y, x);
                }
                else
                {
                    addist = 10 + calculateHValue(y, x);
                }
                f = CurNode->f + addist;

                // Update the f value of node
                if(_LabelMap.at<uchar>(y, x) == free)
                {
                    Node* node = new Node();
                    node->point = Point(x, y);
                    node->parent = CurNode;
                    node->f = f;
                    OpenList.push_back(node);
                    _LabelMap.at<uchar>(y, x) = inOpenList;
                }
                else // _LabelMap.at<uchar>(y, x) == inOpenList
                {
                    // Find the node
                    Node* node = NULL;
                    int length = OpenList.size();
                    for(int i = 0;i < length;i++)
                    {
                        if(OpenList[i]->point.x ==  x && OpenList[i]->point.y ==  y)
                        {
                            node = OpenList[i];
                            break;
                        }
                    }
                    if(f < node->f)
                    {
                        node->f = f;
                        node->parent = CurNode;
                    }
                }

        	}

        }

        //----------- 6th Successor (North-West) ------------ 
  
        // Only process this cell if this is a valid one 
        if (isValid (curY-1, curX-1) == true) 
        {
        	int y = curY-1;
        	int x = curX-1;
        	if(_LabelMap.at<uchar>(y, x) == free || _LabelMap.at<uchar>(y, x) == inOpenList)   // check if (y,x) is inopenlist
        	{
        		// Calculate distance
                double addist, f,g,h;
                if(abs(x - curX) == 1 && abs(y - curY) == 1)
                {
                    addist = 14 + calculateHValue(y, x);
                }
                else
                {
                    addist = 10 + calculateHValue(y, x);
                }
                f = CurNode->f + addist;

                // Update the f value of node
                if(_LabelMap.at<uchar>(y, x) == free)
                {
                    Node* node = new Node();
                    node->point = Point(x, y);
                    node->parent = CurNode;
                    node->f = f;
                    OpenList.push_back(node);
                    _LabelMap.at<uchar>(y, x) = inOpenList;
                }
                else // _LabelMap.at<uchar>(y, x) == inOpenList
                {
                    // Find the node
                    Node* node = NULL;
                    int length = OpenList.size();
                    for(int i = 0;i < length;i++)
                    {
                        if(OpenList[i]->point.x ==  x && OpenList[i]->point.y ==  y)
                        {
                            node = OpenList[i];
                            break;
                        }
                    }
                    if(f < node->f)
                    {
                        node->f = f;
                        node->parent = CurNode;
                    }
                }

        	}

        }

        //----------- 7th Successor (South-East) ------------ 
  
        // Only process this cell if this is a valid one 
        if (isValid(curY+1, curX+1) == true) 
        {
        	int y = curY +1;
        	int x = curX +1;
        	if(_LabelMap.at<uchar>(y, x) == free || _LabelMap.at<uchar>(y, x) == inOpenList)   // check if (y,x) is inopenlist
        	{
        		// Calculate distance
                double addist, f,g,h;
                if(abs(x - curX) == 1 && abs(y - curY) == 1)
                {
                    addist = 14 + calculateHValue(y, x);
                }
                else
                {
                    addist = 10 + calculateHValue(y, x);
                }
                f = CurNode->f + addist;

                // Update the f value of node
                if(_LabelMap.at<uchar>(y, x) == free)
                {
                    Node* node = new Node();
                    node->point = Point(x, y);
                    node->parent = CurNode;
                    node->f = f;
                    OpenList.push_back(node);
                    _LabelMap.at<uchar>(y, x) = inOpenList;
                }
                else // _LabelMap.at<uchar>(y, x) == inOpenList
                {
                    // Find the node
                    Node* node = NULL;
                    int length = OpenList.size();
                    for(int i = 0;i < length;i++)
                    {
                        if(OpenList[i]->point.x ==  x && OpenList[i]->point.y ==  y)
                        {
                            node = OpenList[i];
                            break;
                        }
                    }
                    if(f < node->f)
                    {
                        node->f = f;
                        node->parent = CurNode;
                    }
                }

        	}

        }

        //----------- 8th Successor (South-West) ------------ 
  
        // Only process this cell if this is a valid one 
        if (isValid (curY-1, curX+1) == true) 
        {
        	int y = curY-1;
        	int x = curX+1;
        	if(_LabelMap.at<uchar>(y, x) == free || _LabelMap.at<uchar>(y, x) == inOpenList)   // check if (y,x) is inopenlist
        	{
        		// Calculate distance
                double addist, f,g,h;
                if(abs(x - curX) == 1 && abs(y - curY) == 1)
                {
                    addist = 14 + calculateHValue(y, x);
                }
                else
                {
                    addist = 10 + calculateHValue(y, x);
                }
                f = CurNode->f + addist;

                // Update the f value of node
                if(_LabelMap.at<uchar>(y, x) == free)
                {
                    Node* node = new Node();
                    node->point = Point(x, y);
                    node->parent = CurNode;
                    node->f = f;
                    OpenList.push_back(node);
                    _LabelMap.at<uchar>(y, x) = inOpenList;
                }
                else // _LabelMap.at<uchar>(y, x) == inOpenList
                {
                    // Find the node
                    Node* node = NULL;
                    int length = OpenList.size();
                    for(int i = 0;i < length;i++)
                    {
                        if(OpenList[i]->point.x ==  x && OpenList[i]->point.y ==  y)
                        {
                            node = OpenList[i];
                            break;
                        }
                    }
                    if(f < node->f)
                    {
                        node->f = f;
                        node->parent = CurNode;
                    }
                }

        	}

        }
    }

    return NULL; // Can not find a valid path
}

void Astar::GetPath(Node* TailNode, vector<Point>& path)
{
    PathList.clear();
    path.clear();

    // Save path to PathList
    Node* CurNode = TailNode;
    while(CurNode != NULL)
    {
        PathList.push_back(CurNode);
        CurNode = CurNode->parent;
    }

    // Save path to vector<Point>
    int length = PathList.size();
    for(int i = 0;i < length;i++)
    {
        path.push_back(PathList.back()->point);
        PathList.pop_back();
    }
}

}

/*An explicit A* search constructs the entire graph from the grid cells before begin-
ning the search for a path from start to goal, and is therefore very memory-intensive.

Regardless of the grid con-
figuration, we require some way of mapping the grid to a graph-based representation.
The simplest way of doing this on a regular grid is to associate a node of the graph
with the center of each grid cell.

The nodes are added to the OPEN list with a crucial piece of information. The
f -value of a node determines the order in which it is extracted from the OPEN list.

*/
