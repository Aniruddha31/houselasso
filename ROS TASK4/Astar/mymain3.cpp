#include <iostream>
#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>  //Receive the map via this topic.  Publish the inflation map(mask) via this topic.
//#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>   // to Receive the start point 
#include <geometry_msgs/PoseStamped.h>   //to Receive the target point
#include <geometry_msgs/Quaternion.h>
//#include <Vector3.h>
#include <tf/tf.h>
#include <opencv2/opencv.hpp>
#include "astar.h"
#include "myOccMapTransform.h"
#include <cmath>
#include <tf2/LinearMath/Quaternion.h>

using namespace cv;
using namespace std;

// my define

//my define


//-------------------------------- Global variables ---------------------------------//
// Subscriber
ros::Subscriber map_sub;
ros::Subscriber startPoint_sub;
ros::Subscriber targetPoint_sub;
// Publisher
ros::Publisher mask_pub;
//ros::Publisher path_pub;
ros::Publisher path_pub2; //add

// Object
nav_msgs::OccupancyGrid OccGridMask;
///nav_msgs::Path path;                   //  path is of type nav_msgs::Path   change it 1 below
geometry_msgs::PoseArray my_pose_array; 
pathplanning::AstarConfig config;
pathplanning::Astar astar;
OccupancyGridParam OccGridParam;    // OccupancyGridParam is class in OccMapTransform.h
Point startPoint, targetPoint;

// Parameter

bool map_flag;
bool startpoint_flag;
bool targetpoint_flag;
bool start_flag;
int rate;               //The rate of publishing mask topic.

//-------------------------------- Callback function ---------------------------------//
void MapCallback(const nav_msgs::OccupancyGrid& msg)
{
    // Get parameter
    OccGridParam.GetOccupancyGridParam(msg);

    // Get map in 2D from occupancy grid i.e. 1D
    int height = OccGridParam.height;
    int width = OccGridParam.width;
    int OccProb;
    Mat Map(height, width, CV_8UC1);  //in OpenCV, is there a difference between CV_8U and CV_8UC1? Do they both refer to an 8-bit unsigned type with one channel? => yes
    for(int i=0;i<height;i++)
    {
        for(int j=0;j<width;j++)
        {
            OccProb = msg.data[i * width + j];
            OccProb = (OccProb < 0) ? 100 : OccProb; // set Unknown to 0
            // The origin of the OccGrid is on the bottom left corner of the map
            Map.at<uchar>(height-i-1, j) = 255 - round(OccProb * 255.0 / 100.0);   //OpenCV uses "Round half to even" rounding mode. If fraction is 0.5, it rounds to the nearest even integer. That's why 82.5 is rounded to 82 and 69.5 to 70
        }
    }

    // Initial Djkstra
    Mat Mask;
    
    astar.InitAstar(Map, Mask, config);

    // Publish Mask
    OccGridMask.header.stamp = ros::Time::now();    //uint32 seq ,time stamp,string frame_id
    OccGridMask.header.frame_id = "map";
    OccGridMask.info = msg.info;
    for(int i=0;i<height;i++)
    {
        for(int j=0;j<width;j++)
        {
            OccProb = Mask.at<uchar>(height-i-1, j) * 255;
            OccGridMask.data.push_back(OccProb);
        }
    }

    // Set flag
    map_flag = true;
    startpoint_flag = false;
    targetpoint_flag = false;
}

void StartPointCallback(const geometry_msgs::PoseWithCovarianceStamped& msg)
{
    Point2d src_point = Point2d(msg.pose.pose.position.x, msg.pose.pose.position.y);
    OccGridParam.Map2ImageTransform(src_point, startPoint);

    // Set flag
    startpoint_flag = true;
    if(map_flag && startpoint_flag && targetpoint_flag)
    {
        start_flag = true;
    }

//    ROS_INFO("startPoint: %f %f %d %d", msg.pose.pose.position.x, msg.pose.pose.position.y,
//             startPoint.x, startPoint.y);
}

void TargetPointtCallback(const geometry_msgs::PoseStamped& msg)
{
    Point2d src_point = Point2d(msg.pose.position.x, msg.pose.position.y);
    OccGridParam.Map2ImageTransform(src_point, targetPoint);

    // Set flag
    targetpoint_flag = true;
    if(map_flag && startpoint_flag && targetpoint_flag)
    {
        start_flag = true;
    }

//    ROS_INFO("targetPoint: %f %f %d %d", msg.pose.position.x, msg.pose.position.y,
//             targetPoint.x, targetPoint.y);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

          /* tfScalar angle(const Vector3& v) const 
           {
                 tfScalar s = tfSqrt(length2() * v.length2());
                 tfFullAssert(s != tfScalar(0.0));
                 return tfAcos(dot(v) / s);
           }

           tfScalar funct2(const Vector3& v) const 
           {
                 tfScalar s = tfSqrt(length2() * v.length2());
                 
                 return s;
           }*/

     /* Vector3::normalized() 
      {
           return *this / length();   // q.normalize();
      }*/

     tf::Vector3 orthogonal(tf::Vector3 v)
      {
           float x = v.getX();
           float y = v.getY();
           float z = v.getZ();
     
           tf::Vector3 X_AXIS(1 , 0, 0);
           tf::Vector3 Y_AXIS(0 , 1, 0);
           tf::Vector3 Z_AXIS(0 , 0, 1);

           tf::Vector3 other = x < y ? (x < z ? X_AXIS  : Z_AXIS) : (y < z ? Y_AXIS : Z_AXIS);
           return tfCross(v, other);
      }

           tf::Quaternion get_rotation_between(tf::Vector3 u, tf::Vector3 v)
                    {
                        tfScalar k_cos_theta = tfDot(u,v);
                        tfScalar k = tfSqrt(u.length2() * v.length2());

                        if (k_cos_theta / k == -1)
                        {
                            // 180 degree rotation around any orthogonal vector
                            return tf::Quaternion(orthogonal(u), 0).normalize();
                        }

                        return tf::Quaternion( tfCross(u, v),k_cos_theta + k).normalize();
                    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//-------------------------------- Main function ---------------------------------//
int main(int argc, char * argv[])
{
    //  Initial node
    ros::init(argc, argv, "astar");     //This is also where we specify the name of our node. Node names must be unique in a running system. 
    ros::NodeHandle nh;   //A NodeHandle is an object which represents your ROS node. You usually only need one or two node handles
    ros::NodeHandle nh_priv("~");  // // private node handle
    ROS_INFO("Start astar node!\n");

    // Initial variables
    map_flag = false;
    startpoint_flag = false;
    targetpoint_flag = false;
    start_flag = false;

    // Parameter   that are set using mydijkstra.launch file
    //nh.param<std::string>("param_name", variable, "value");  checks if param_name is exist the the command pass the value of param in variable but if the param doesn't exist then the command pass "value" to variable
    nh_priv.param<int>("OccupyThresh", config.OccupyThresh, -1);
    
    nh_priv.param<int>("rate", rate, 10);     // loop at 10Hz  ,  10Hz means you need to read the data every 100ms. If it takes 17ms to read the data, then the program needs to sleep for 83ms until the next read. And you have to compute that for each iteration of the loop. Sometimes the reading will take 17ms, but maybe sometimes 19ms or 16ms.

    // Subscribe topics
    map_sub = nh.subscribe("map", 10, MapCallback);  //Subscribe to the "map" topic with the master. ROS will call the MapCallback() function whenever a new message arrives. The 2nd argument is the queue size, in case we are not able to process messages fast enough. In this case, if the queue reaches 10 messages, we will start throwing away old messages as new ones arrive
    startPoint_sub = nh.subscribe("initialpose", 10, StartPointCallback); // //Receive the start point via this topic
    targetPoint_sub = nh.subscribe("move_base_simple/goal", 10, TargetPointtCallback); // //Receive the target point via this topic.

    // Advertise topics
    mask_pub = nh.advertise<nav_msgs::OccupancyGrid>("mask", 1);
    
    path_pub2 = nh.advertise<geometry_msgs::PoseArray>("nav_path", 10) ;

    // Loop and wait for callback
    ros::Rate loop_rate(rate);
    while(ros::ok())
    {
        if(start_flag)
        {
            // Start planning path
            vector<Point> PathList;
            astar.PathPlanning(startPoint, targetPoint, PathList);
            if(!PathList.empty())
            {
                
                my_pose_array.header.stamp = ros::Time::now();
                
                my_pose_array.header.frame_id = "map";
                //path.poses.clear();
                my_pose_array.poses.clear();
                for(int i=0;i<PathList.size();i++)
                {
                    Point2d dst_point;
                    OccGridParam.Image2MapTransform(PathList[i], dst_point);

                    
                    geometry_msgs::Pose p;
                    
                    
                
                    p.position.x = dst_point.x;
                    //pose_stamped.pose.position.y = dst_point.y;
                    p.position.y = dst_point.y;
                    //pose_stamped.pose.position.z = 0;
                    p.position.z = 0;
      
                    //add my extra
                    tf::Vector3 u(-1 , 0, 0);

                    if(targetPoint.x > PathList[i].x)
                    tf::Vector3 u(-1 , 0, 0);
                    else
                        tf::Vector3 u(1 , 0, 0);
                    tf::Vector3 v(PathList[i+1].x-PathList[i].x, PathList[i+1].y-PathList[i].y, 0);

                    //tf::Quaternion qprev =  get_rotation_between( u,  v);

                    tf::Quaternion q =  get_rotation_between( u,  v); //PathList[i].orientation;
                    if(!isnan(q[0]) && !isnan(q[1]) && !isnan(q[2]) && !isnan(q[3]) )
                    {
                          p.orientation.x = q[0];
                          p.orientation.y = q[1];
                          p.orientation.z = q[2];
                          p.orientation.w = q[3];//q.w
                    // add my extra   
                    }  
                    /*else if(!isnan(qprev[0]) && !isnan(qprev[1]) && !isnan(qprev[2]) && !isnan(qprev[3]))
                    {
                          p.orientation.x = qprev[0];
                          p.orientation.y = qprev[1];
                          p.orientation.z = qprev[2];
                          p.orientation.w = qprev[3];//q.w
                    }*/
                    else
                    {
                          p.orientation.x = 0;
                          p.orientation.y = 0;
                          p.orientation.z = 0;
                          p.orientation.w = 1;//q.w
                    }               

                
                    my_pose_array.poses.push_back(p);
                }
                
                path_pub2.publish(my_pose_array);

                ROS_INFO("Find a valid path successfully");
            }
            else
            {
                ROS_ERROR("Can not find a valid path");
            }

            // Set flag
            start_flag = false;
        }

        if(map_flag)
        {
            mask_pub.publish(OccGridMask);
        }

        loop_rate.sleep();
        ros::spinOnce();
    }


    return 0;
}
