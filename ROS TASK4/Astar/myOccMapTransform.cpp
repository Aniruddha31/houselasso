

#include <iostream>
#include <cmath>
#include <nav_msgs/OccupancyGrid.h>
#include <tf/tf.h>
#include <opencv2/opencv.hpp>


using namespace std;
using namespace cv;


//-------------------------------- Class---------------------------------//
class OccupancyGridParam{

public: // Interface
    void GetOccupancyGridParam(nav_msgs::OccupancyGrid OccGrid);
    void Image2MapTransform(Point& src_point, Point2d& dst_point);
    void Map2ImageTransform(Point2d& src_point, Point& dst_point);

private: // Private function

public:  // Public variable
    double resolution;                   //used to increase cell size in an occupancy grid
    int height;
    int width;
    // Origin pose
    double x;
    double y;
    double theta;

private: // Private variable
    // Transform
    Mat R;                                                //opencv Mat has two parts header for matrix size and pointer used for storing
    Mat t;

};




void OccupancyGridParam::GetOccupancyGridParam(nav_msgs::OccupancyGrid OccGrid)
{
    // Get parameter
    resolution = OccGrid.info.resolution;
    height = OccGrid.info.height;
    width = OccGrid.info.width;
    x = OccGrid.info.origin.position.x;
    y = OccGrid.info.origin.position.y;

    double roll, pitch, yaw;
    geometry_msgs::Quaternion q = OccGrid.info.origin.orientation;
    tf::Quaternion quat(q.x, q.y, q.z, q.w); // x, y, z, w    eq=> q = x + yi + zj + wk  && i, j, k which satisfy the rules i^2 = j^2 = k^2 = i j k = −1 &&  quaternion multiplication rule: v → w → = v → × w → − v → ⋅ w → 
    tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);
    theta = yaw;

    // Calculate R, t                     // R  => rotate ,  t => translate
    R = Mat::zeros(2,2, CV_64FC1);         // initialize Mat size and fill it with zeros in a class
    R.at<double>(0, 0) = resolution * cos(theta);   //Affine transformation , rotation of image by theta about its center
    R.at<double>(0, 1) = resolution * sin(-theta);
    R.at<double>(1, 0) = resolution * sin(theta);
    R.at<double>(1, 1) = resolution * cos(theta);
    t = Mat(Vec2d(x, y), CV_64FC1);
}

void OccupancyGridParam::Image2MapTransform(Point& src_point, Point2d& dst_point)// src =>source , dst => destination
{
    // Upside down
    Mat P_src = Mat(Vec2d(src_point.x, height - 1 - src_point.y), CV_64FC1);  //matrix P_src is of shape R = shape of t =[2,2]
    // Rotate and translate
    Mat P_dst = R * P_src + t; //( Matrix multiplication in opencv using "*" operator) #changed
    //my add extra

    //Mat P_dst = P_src;
    //my add extra 
    dst_point.x = P_dst.at<double>(0, 0);  //changed
    dst_point.y = P_dst.at<double>(1, 0);  //changed
}

void OccupancyGridParam::Map2ImageTransform(Point2d& src_point, Point& dst_point)
{
    Mat P_src = Mat(Vec2d(src_point.x, src_point.y), CV_64FC1);
    // Rotate and translate
     Mat P_dst = R.inv() * (P_src - t);
    // Upside down
    //my add extra

    //Mat P_dst = P_src;
    

    dst_point.x = round(P_dst.at<double>(0, 0));
    dst_point.y = height - 1 - round(P_dst.at<double>(1, 0));
}
