#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <vector>
#include <math.h>
#include <cuda.h>
#include <cuda_runtime.h>
using namespace std;

class CSR
{
public:
	// construct Ap, Aj, Av
	vector<int> Ap;
        vector<int> Aj;
        vector<float> Av;
        int d_X;
        int d_Y;
        int P,J,V;

	// CSR Constructor
	CSR(vector<vector<float>> const &rows)
	{
		// 
		d_X=rows.size(); // size of outer vector
                cout << "\nd_X is:" << d_X;
                d_Y=rows[0].size();
                cout << "\nd_Y is:" << d_Y;
                Ap.push_back(0);  //initial zero is required
                int count=0;

                //---------LOOP MATRIX create Av and Aj-----------
        
                for(int i=0; i<d_X;i++  )
                {
                    for(int j=0; j<d_Y; j++) //iterate full row "i" [i,j]
                    {
                        if(rows[i][j]!=0)
                        {
                            Av.push_back(rows[i][j]);
                            Aj.push_back(j);
                            
                            count = count+1;
                        }
                    }
                    Ap.push_back(count);
                }
          
                //------------END LOOP-----------------------------
 
                P = d_X +1;
                J = V = count;

                //-------------CHECK VALUES------------------------
                cout << "\nAv is:\n";
                for(int i=0; i< Av.size();i++)
                     cout << Av[i] << "\t";

                cout << "\nAj is:\n";
                for(int i=0; i< Aj.size();i++)
                     cout << Aj[i] << "\t";

                cout << "\nAp is:\n";
                for(int i=0; i< Ap.size();i++)
                     cout << Ap[i] << "\t";

                cout << "\n";
	}
};

