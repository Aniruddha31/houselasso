#include "csr_matrix.h"
#include <cuda_fp16.h> // for __shfl_down_sync() function
                       //       1)permit exchanging of a variable between threads within a warp without use of shared memory

//------------------------- CONSTANT HEADERS------------------------------//
#define WARP_MASK 0xffffffff

//------------------------------------------------------------------------//

template <class T>
__device__ T warp_reduce (T val)
{
   for(int offset = warpSize/2; offset>0 ; offset=offset/2)
       val = val + __shfl_down_sync(WARP_MASK, val, offset);

   return val;
}

__global__ void csrmul_kernel(int *Ap, int *Aj,float *Av, int num_rows, float *x, float *y, int dimY2, int dimY , int special,int outer)
{
    
 
       int thread_id = blockIdx.x*blockDim.x + threadIdx.x;
       int warp_id = thread_id / 32;
       int lane = thread_id % 32;
       int row = warp_id; // <<-----one warp per row !!!------>>

       float sum = 0;

       if( row<num_rows ) // num_rows = rows of csr 
       {
          int row_begin = Ap[row];
          int row_end = Ap[row+1];

          //y[row] = multiply_row(row_end-row_begin, Aj+row_begin,Av+row_begin, x); 
          for(int ele = row_begin + lane; ele < row_end; ele = ele + 32)
              sum = sum + Av[ele] * x[Aj[ele] + special* dimY/*"dimX of 2d mat"*/ ];
       }
       sum = warp_reduce (sum);
   
       if( lane == 0 && row < num_rows)
          y[row + num_rows*outer] = sum;
   
}



int main()
{
    int i,j;

    //int    *Ap,*Aj;
    int    *d_Ap,*d_Aj;
    float  /**Av,*/  *y;
    float  *d_Av, *d_x, *d_y; 

    //-------------------------input matrix--------------------------------//
    int dimX , dimY, ele;

    // construct a vector of vectors to represent a matrix
    vector<vector<float>> rows;

    cout << "Enter matrix dim x-axis/y-axis\n";
    cin >> dimX >> dimY;

  
    cout << "Enter matrix rowise\n";
    for(i=0;i<dimX;i++)
    {
        rows.resize(dimX);
        for(j=0;j<dimY;j++)
        {
            cin >> ele ;
            rows[i].push_back(ele);
        }
    }

    //--------------------------CREATE CSR MATRIX--------------------------
    CSR csr(rows);

    //----------------------- Initialize X- array------------------
    vector<float> x;
    int dimY2;
    cout << "Enter matrix dim y-axis\n";
    cin >> dimY2;
    cout << "Enter matrix2 columnwise\n";
    for(int j = 0; j < dimY2; j++){
        cout << "enter column  " << j << "\n";
        for(i=0; i< dimY ;i++)
        {
            cin >> ele;
            //x[j* dimY + i] = ele;
            x.push_back(ele);
        }
    }


    //------------------------ Allocate host memory-------------------
    y = (float*)malloc(sizeof(float) * (dimX *dimY2));
    
   
    //----------------------- Allocate device memory -----------------
    cudaMalloc((void**)&d_Ap, sizeof(int  ) * csr.P        );
    cudaMalloc((void**)&d_Aj, sizeof(int  ) * csr.J        );
    cudaMalloc((void**)&d_Av, sizeof(float) * csr.V        );
    cudaMalloc((void**)&d_x , sizeof(float) * (dimY *dimY2));
    cudaMalloc((void**)&d_y , sizeof(float) * (dimX *dimY2));

    //------------- Transfer data from host to device memory-----------
    cudaMemcpy(d_Ap, &csr.Ap[0], sizeof(int  ) * csr.P         , cudaMemcpyHostToDevice);
    cudaMemcpy(d_Aj, &csr.Aj[0], sizeof(int  ) * csr.J         , cudaMemcpyHostToDevice);
    cudaMemcpy(d_Av, &csr.Av[0], sizeof(float) * csr.V         , cudaMemcpyHostToDevice);
    cudaMemcpy(d_x , &x[0],      sizeof(float) * (dimY *dimY2) , cudaMemcpyHostToDevice);

    //--------------------------- Executing kernel ------------------
    int block_size = 256;
    int grid_size = ((dimX + block_size -1) / block_size);
    int special;
    for(int outer =0 ;outer < dimY2/*"dimY of 2d mat"*/; outer++)
    {
        special = outer;
        csrmul_kernel<<<grid_size,block_size>>>(d_Ap, d_Aj,d_Av, dimX, d_x, d_y, dimY2, dimY, special, outer);
    }
    //gridsize = no. of blocks per grid and blocksize = no .of threads per block
    
    //----------------- Transfer data back to host memory---------------
    cudaMemcpy(y, d_y, sizeof(float) * (dimX *dimY2), cudaMemcpyDeviceToHost);

    //---------------------------- Verification---------------------
    cout << "matrix product in column major is: \n";
    for( i = 0; i < (dimX *dimY2); i++){
        printf("%f\t",y[i]);
    }

    printf("PASSED\n");

    // Deallocate device memory
    cudaFree(d_Ap);
    cudaFree(d_Av);
    cudaFree(d_Aj);
    cudaFree(d_x);
    cudaFree(d_y);

    // Deallocate host memory
    free(&csr.Ap[0]); 
    free(&csr.Av[0]); 
    free(&csr.Aj[0]);
    free(&x[0]);
    free(y);
}


/*
0
2
2
5
7
3
1
2
4
1
1
1
0
2
1
2
3
0
3
4.000000	0.000000	7.000000	2.000000	PASSED

real	1m0.762s
user	0m0.076s
sys	0m0.160s

Enter matrix rowise
3
0
1
0
0
0
0
0
0
2
4
1
1
0
0
1

Enter matrix rowise
1
2
3
4
5
6
7
8
9
0
1
2
3
4
5
6
7
8
9
0
1
2
3
4
5


*/






















