#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <vector>
#include <math.h>
#include <cuda.h>
#include <cuda_runtime.h>
using namespace std;

class CSR
{
public:
	// construct Ap, Aj, Av
	vector<int> Ap;
        vector<int> Aj;
        vector<float> Av;
        int d_X;
        int d_Y;
        int P,J,V;

	// CSR Constructor
	CSR(vector<vector<int>> const &rows)
	{
		// 
		d_X=rows.size(); // size of outer vector
                cout << "\nd_X is:" << d_X;
                d_Y=rows[0].size();
                cout << "\nd_Y is:" << d_Y;
                Ap.push_back(0);  //initial zero is required
                int count=0;

                //---------LOOP MATRIX create Av and Aj-----------
        
                for(int i=0; i<d_X;i++  )
                {
                    for(int j=0; j<d_Y; j++)
                    {
                        if(rows[i][j]!=0)
                        {
                            Av.push_back(rows[i][j]);
                            Aj.push_back(j);
                            
                            count = count+1;
                        }
                    }
                    Ap.push_back(count);
                }
          
                //------------END LOOP-----------------------------
 
                P = d_X +1;
                J = V = count;

                //-------------CHECK VALUES------------------------
                cout << "\nAv is:\n";
                for(int i=0; i< Av.size();i++)
                     cout << Av[i] << "\t";

                cout << "\nAj is:\n";
                for(int i=0; i< Aj.size();i++)
                     cout << Aj[i] << "\t";

                cout << "\nAp is:\n";
                for(int i=0; i< Ap.size();i++)
                     cout << Ap[i] << "\t";

                cout << "\n";
	}
};


__device__ float multiply_row(int rowsize, int *Aj, float *Av,float *x)
{
   float sum = 0;
   for( int column=0; column<rowsize; ++column)
   sum += Av[column] * x[Aj[column]];
   return sum;
}

__global__ void csrmul_kernel(int *Ap, int *Aj,float *Av, int num_rows, float *x, float *y)
{
   int row = blockIdx.x*blockDim.x + threadIdx.x;
   if( row<num_rows )
   {
      int row_begin = Ap[row];
      int row_end = Ap[row+1];

      y[row] = multiply_row(row_end-row_begin, Aj+row_begin,Av+row_begin, x);
   }
}

int main()
{
    int i,j;

    //int    *Ap,*Aj;
    int    *d_Ap,*d_Aj;
    float  /**Av,*/ *x, *y;
    float  *d_Av, *d_x, *d_y; 

    //-------------------------input matrix--------------------------------//
    int dimX , dimY, ele;
    int X,Y;

    // construct a vector of vectors to represent a matrix
    vector<vector<int>> rows;

    cout << "Enter matrix dim x-axis/y-axis\n";
    cin >> dimX >> dimY;

    //-----------
    X = dimX;
    Y = dimY;
    int num_rows= dimX;
    //-----------
    cout << "Enter matrix rowise\n";
    for(i=0;i<dimX;i++)
    {
        rows.resize(dimX);
        for(j=0;j<dimY;j++)
        {
            cin >> ele ;
            rows[i].push_back(ele);
        }
    }

    //--------------------------CREATE CSR MATRIX--------------------------
    CSR csr(rows);

    //------------------------ Allocate host memory-------------------
    y    = (float*)malloc(sizeof(float) *     Y);
    
    //----------------------- Initialize X- array------------------
    for(int i = 0; i < X; i++){
        x[i] = 1.0f;
    }

    //----------------------- Allocate device memory -----------------
    cudaMalloc((void**)&d_Ap, sizeof(int  ) * csr.P);
    cudaMalloc((void**)&d_Aj, sizeof(int  ) * csr.J);
    cudaMalloc((void**)&d_Av, sizeof(float) * csr.V);
    cudaMalloc((void**)&d_x , sizeof(float) *     X);
    cudaMalloc((void**)&d_y , sizeof(float) *     Y);

    //------------- Transfer data from host to device memory-----------
    cudaMemcpy(d_Ap, &csr.Ap[0], sizeof(int  ) * csr.P, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Aj, &csr.Aj[0], sizeof(int  ) * csr.J, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Av, &csr.Av[0], sizeof(float) * csr.V, cudaMemcpyHostToDevice);
    cudaMemcpy(d_x, x,        sizeof(float) *     X, cudaMemcpyHostToDevice);

    //--------------------------- Executing kernel ------------------
    int block_size = 256;
    int grid_size = ((X + block_size -1) / block_size);
    csrmul_kernel<<<grid_size,block_size>>>(d_Ap, d_Aj,d_Av, num_rows, d_x, d_y);
    
    
    //----------------- Transfer data back to host memory---------------
    cudaMemcpy(y, d_y, sizeof(float) * Y, cudaMemcpyDeviceToHost);

    //---------------------------- Verification---------------------
    for( i = 0; i < Y; i++){
        printf("%f\t",y[i]);
    }

    printf("PASSED\n");

    // Deallocate device memory
    cudaFree(d_Ap);
    cudaFree(d_Av);
    cudaFree(d_Aj);
    cudaFree(d_x);
    cudaFree(d_y);

    // Deallocate host memory
    free(&csr.Ap[0]); 
    free(&csr.Av[0]); 
    free(&csr.Aj[0]);
    free(x);
    free(y);
}


/*
0
2
2
5
7
3
1
2
4
1
1
1
0
2
1
2
3
0
3
4.000000	0.000000	7.000000	2.000000	PASSED

real	1m0.762s
user	0m0.076s
sys	0m0.160s
*/






















