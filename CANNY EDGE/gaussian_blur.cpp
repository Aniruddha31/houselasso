#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <string>
#include <opencv2/imgproc/imgproc.hpp>
#include <cuda_runtime.h>
#include "2d_to_row_major.h"
#include "gauss_fil.h"
#include "csr_matrix.h"
#include <cuda_fp16.h>// for __shfl_down_sync() function
                       //       1)permit exchanging of a variable between threads within a warp without use of shared memory


using namespace cv;
using namespace std;

//<---------------------------------DEFINE SOBEL FILTERS-------------------------------->//
std::vector<vector<float>> GXfilter {{-1,0,1},{-2,0,2},{-1,0,1} }; // gradient in X => edge in Y
std::vector<vector<float>> GYfilter {{-1,-2,-1},{0,0,0},{1,2,1} }; // gradient in Y => edge in X

//<---------------------------------DEFINE GAUSSIAN FILTERS----------------------------->//
//std::vector<vector<float>> G {{1/6,2/6,1/6},{2/6,4/6,2/6},{1/6,2/6,1} };




//############################################################## PREVIOUS CUDA CODE ###############################################>

//------------------------- CONSTANT HEADERS------------------------------//
#define WARP_MASK 0xffffffff

//------------------------------------------------------------------------//


template <class T>
__device__ T warp_reduce (T val)
{
   for(int offset = warpSize/2; offset>0 ; offset=offset/2)
       val = val + __shfl_down_sync(WARP_MASK, val, offset);

   return val;
}

__global__ void csrmul_kernel(int *Ap, int *Aj,float *Av, int num_rows, float *x, float *y)
{
   int thread_id = blockIdx.x*blockDim.x + threadIdx.x;
   int warp_id = thread_id / 32;
   int lane = thread_id % 32;
   int row = warp_id; // <<-----one warp per row !!!------>>

   float sum = 0;

   if( row<num_rows )
   {
      int row_begin = Ap[row];
      int row_end = Ap[row+1];

      //y[row] = multiply_row(row_end-row_begin, Aj+row_begin,Av+row_begin, x); 
      for(int ele = row_begin + lane; ele < row_end; ele = ele + 32)
          sum = sum + Av[ele] * x[Aj[ele]];
   }
   sum = warp_reduce (sum);
   
   if( lane == 0 && row < num_rows)
      y[row] = sum;
}

//############################################################## PREVIOUS CUDA CODE ###############################################>




int main( )
{

    Mat image;
    image = imread("index.jpeg", CV_LOAD_IMAGE_COLOR);   // Read the file

    cv::Mat greyMat,greyMatpadded;
    cv::cvtColor(image, greyMat, cv::COLOR_BGR2GRAY);
    greyMat.convertTo(greyMat,CV_32F);

    // Initialize arguments for PADDING
    int PAD =2;
    int top = (int) (greyMat.rows + PAD); 
    int bottom = top;
    int left = (int) (greyMat.cols + PAD); 
    int right = left;

    cout << "\n greyMat.cols): " << greyMat.cols << "\n";

    //copyMakeBorder( greyMat, greyMatpadded, top, bottom, left, right, BORDER_CONSTANT, 255 );

    //imshow( "Display window", greyMat );

    // convert to float vector
    std::vector<float> array((float*)greyMat.data, (float*)greyMat.data + greyMat.rows * greyMat.cols);

    

    /*for(int i =0; i<array.size(); i++)
    {
        cout << array[i] << "\t";
    }*/

    //works fine till here

    //<--------------------------------------CREATING GAUSSIAN FILTER AND CONVERTING IT TO SPARSE MATRIX------------------>
  
    gauss_fil GAUSS((float)1.0,(float)2.0,array.size(),(int)greyMat.cols,(int)greyMat.rows );

    //<---------------------------------------CONVERTING spG to CSR format i.e. spG_csr----------------------------------->

    //cout << "\n spG[0][0] : \n" << GAUSS.spG.size();

    //works fine till here

    cout << "\n\n\n\n\n <-------------------- CONVERTING spG to spG_CSR  -------------------> \n\n\n\n\n\n\n";

    CSR spG_csr(GAUSS.spG);
    //works fine till here


    
    //############################################################# PREVIOUS CUDA CODE ##############################################
    int    *d_Ap,*d_Aj;
    float  /**Av,*/ *x, *y;
    float  *d_Av, *d_x, *d_y; 



    int X,Y,i;
    X = array.size();
    Y = array.size();
    int num_rows= array.size();
    //------------------------ Allocate host memory-------------------
    y    = (float*)malloc(sizeof(float) *     Y);
    
    //----------------------- Initialize X- array------------------
    /*for(int i = 0; i < X; i++){
        x[i] = 1.0f;
    }*/



    //----------------------- Allocate device memory -----------------
    cudaMalloc((void**)&d_Ap, sizeof(int  ) * spG_csr.P);
    cudaMalloc((void**)&d_Aj, sizeof(int  ) * spG_csr.J);
    cudaMalloc((void**)&d_Av, sizeof(float) * spG_csr.V);
    cudaMalloc((void**)&d_x , sizeof(float) *         X);
    cudaMalloc((void**)&d_y , sizeof(float) *         Y);

    //------------- Transfer data from host to device memory-----------
    cudaMemcpy(d_Ap, &spG_csr.Ap[0], sizeof(int  ) * spG_csr.P, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Aj, &spG_csr.Aj[0], sizeof(int  ) * spG_csr.J, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Av, &spG_csr.Av[0], sizeof(float) * spG_csr.V, cudaMemcpyHostToDevice);
    cudaMemcpy(d_x , &array[0]     , sizeof(float) *         X, cudaMemcpyHostToDevice);

     //--------------------------- Executing kernel ------------------
    cout << "\n\n\n\n\n <--------------------EXECUTING CUDA KERNEL -------------------> \n\n\n\n\n\n\n";
    int block_size = 256;
    int grid_size = ((X + block_size -1) / block_size);
    csrmul_kernel<<<grid_size,block_size>>>(d_Ap, d_Aj,d_Av, num_rows, d_x, d_y);
    //gridsize = no. of blocks per grid and blocksize = no .of threads per block
    
    //----------------- Transfer data back to host memory---------------
    cudaMemcpy(y, d_y, sizeof(float) * Y, cudaMemcpyDeviceToHost);

    //---------------------------- Verification---------------------
    for( i = 0; i < Y; i++){
        printf("%f\t",y[i]);
    }

    printf("PASSED\n");

    // Deallocate device memory
    cudaFree(d_Ap);
    cudaFree(d_Av);
    cudaFree(d_Aj);
    cudaFree(d_x);
    cudaFree(d_y);

    // Deallocate host memory
    /*free(&spG_csr.Ap[0]); 
    free(&spG_csr.Av[0]); 
    free(&spG_csr.Aj[0]);
    free(&array[0]);
    free(y);*/






    waitKey(0);                                          // Wait for a keystroke in the window
    return 0;
}