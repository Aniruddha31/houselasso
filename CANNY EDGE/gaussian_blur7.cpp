#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <string>
#include <opencv2/imgproc/imgproc.hpp>
#include <cuda_runtime.h>
#include "2d_to_row_major.h"
#include "gauss_fil.h"
#include "csr_matrix.h"
#include "sobel_filx.h"
#include "sobel_fily.h"
#include <cuda_fp16.h>// for __shfl_down_sync() function
                       //       1)permit exchanging of a variable between threads within a warp without use of shared memory
#include <math.h>
#include <vector_types.h>
#include <cuda.h>
using namespace cv;
using namespace std;

//<---------------------------------DEFINE SOBEL FILTERS-------------------------------->//
std::vector<vector<float>> GXfilter {{-1,0,1},{-2,0,2},{-1,0,1} }; // gradient in X => edge in Y
std::vector<vector<float>> GYfilter {{-1,-2,-1},{0,0,0},{1,2,1} }; // gradient in Y => edge in X



//############################################################## PREVIOUS CUDA CODE ###############################################>

//------------------------- CONSTANT HEADERS------------------------------//
#define WARP_MASK 0xffffffff

//------------------------------------------------------------------------//


// template <class T>
// __device__ T warp_reduce (T val)
// {
//    for(int offset = warpSize/2; offset>0 ; offset=offset/2)
//        val = val + __shfl_down_sync(WARP_MASK, val, offset);

//    return val;
// }

// __global__ void csrmul_kernel(int *Ap, int *Aj,float *Av, int num_rows, float *x, float *y)
// {
//    int thread_id = blockIdx.x*blockDim.x + threadIdx.x;
//    int warp_id = thread_id / 32;
//    int lane = thread_id % 32;
//    int row = warp_id; // <<-----one warp per row !!!------>>

//    float sum = 0;

//    if( row<num_rows )
//    {
//       int row_begin = Ap[row];
//       int row_end = Ap[row+1];

//       //y[row] = multiply_row(row_end-row_begin, Aj+row_begin,Av+row_begin, x); 
//       for(int ele = row_begin + lane; ele < row_end; ele = ele + 32)
//           sum = sum + Av[ele] * x[Aj[ele]];
//    }
//    sum = warp_reduce (sum);
   
//    if( lane == 0 && row < num_rows)
//       y[row] = sum;
// }

__device__ float multiply_row(int rowsize, int *Aj, float *Av,float *x)
{
   float sum = 0;
   for( int column=0; column<rowsize; ++column)
   sum += Av[column] * x[Aj[column]];
   return sum;
}

__global__ void csrmul_kernel(int *Ap, int *Aj,float *Av, int num_rows, float *x, float *y)
{
   int row = blockIdx.x*blockDim.x + threadIdx.x;
   if( row<num_rows )
   {
      int row_begin = Ap[row];
      int row_end = Ap[row+1];

      y[row] = multiply_row(row_end-row_begin, Aj+row_begin,Av+row_begin, x);
   }
}

__global__ void arctan_kernel(float *y2, float *y3,int Y2, float *y)
{

   int row = blockIdx.x*blockDim.x + threadIdx.x;
   if( row < Y2 )
   {
      

      y[row] = atan2((double)y3[row],(double)y2[row]);
   }
}

__global__ void mag_kernel(float *y2, float *y3,int Y2, float *y)
{
   int row = blockIdx.x*blockDim.x + threadIdx.x;
   if( row < Y2 )
   {
      
      y[row] = hypotf((float)y3[row],(float)y2[row]);
   }
}

//  __global__ void NMS_kernel ( float *yatan, float *ymag , int height,int width , float *y, float M_PI )
// {
//    // yatan is in row major :
//    // ymag is in row major  :

   
//    // DO + M_pi/8 everywhere !!!!!!!!!!!!!!!!!!!
//     // NOTE  0 IS FOR EDGE


//    int xid = blockIdx.x*blockDim.x + threadIdx.x; // related to height
//    int yid = blockIdx.y*blockDim.y + threadIdx.y; // related to width

//    int button = (   (xid-1)>0 && (xid+1)<height  && (yid-1)>0 && (yid+1)<width && xid <height && yid<width  ); // boundary conditions
//    int k1,k2,k3,k4;
//    if( button )// TRAVERSING IN ROW MAJOR FORMAT
//    {
//       k1=( yatan[xid*width + yid] <= M_PI/4 && yatan[xid*width + yid] => 0) ||  (yatan[xid*width + yid] <= -3*M_PI/4 && yatan[xid*width + yid] >= -M_PI);
//       /*1*/if( k1  )
//       {

//          if( ymag[xid*width + yid] > ymag[(xid+1)*width + (yid)]  &&  ymag[xid*width + yid] > ymag[(xid-1)*width + (yid)]  )
//          {
//             y[xid*width + yid] = 0;
//          }
//       }

//       /*2*/else if( yatan[xid*width + yid] <= M_PI/2 && yatan[xid*width + yid] => M_PI/4 ||  yatan[xid*width + yid] <= -M_PI/2 && yatan[xid*width + yid] >= -3M_PI/4 )
//       {
//          if( ymag[xid*width + yid] > ymag[(xid-1)*width + (yid-1)] &&  ymag[xid*width + yid] > ymag[(xid+1)*width + (yid+1)]  )
//          {
//             y[xid*width + yid] = 0;
//          }
//       }

//       /*3*/else if( yatan[xid*width + yid] <= 3*M_PI/4 && yatan[xid*width + yid] => M_PI/2 ||  yatan[xid*width + yid] <= -M_PI/4 && yatan[xid*width + yid] >= -M_PI/2 )
//       {
//          if( ymag[xid*width + yid] > ymag[(xid)*width + (yid-1)] &&  ymag[xid*width + yid] > ymag[(xid)*width + (yid+1)]  )
//          {
//             y[xid*width + yid] = 0;
//          }
//       }

//       /*4*/else if( yatan[xid*width + yid] <= M_PI && yatan[xid*width + yid] => 3*M_PI/4 ||  yatan[xid*width + yid] <= 0 && yatan[xid*width + yid] >= -M_PI/4 )
//       {
//          if( ymag[xid*width + yid] > ymag[(xid+1)*width + (yid-1)] &&  ymag[xid*width + yid] > ymag[(xid-1)*width + (yid+1)]  )
//          {
//             y[xid*width + yid] = 0;
//          }
//       }
//       else
//       {
//          y[xid*width + yid] = 254;
//       }
//    }
// }

//############################################################## PREVIOUS CUDA CODE ###############################################>




int main( )
{

    Mat image;
    image = imread("index.jpeg", CV_LOAD_IMAGE_COLOR);   // Read the file

    imshow( "orig", image);

    Mat greyMat,greyMatpadded;
    cvtColor(image, greyMat, CV_RGB2GRAY);
    //greyMat.convertTo(greyMat,CV_32F);

    transpose(greyMat, greyMat);  // USE SINCE my algo works for column major and it will output row major after execution

    // Initialize arguments for PADDING
    int PAD =2;
    int top = (int) (greyMat.rows + PAD); 
    int bottom = top;
    int left = (int) (greyMat.cols + PAD); 
    int right = left;

    cout << "\n greyMat.cols): " << greyMat.cols << "\n";

    //copyMakeBorder( greyMat, greyMatpadded, top, bottom, left, right, BORDER_CONSTANT, 255 );

    imshow( "grey", greyMat );

    // convert to float vector
    std::vector<float> array((float*)greyMat.data, (float*)greyMat.data + greyMat.rows * greyMat.cols);
    // (NOTE THE ABOVE IS COLUMN MAJOR SINCE I TOOK TRANSPOSE ABOVE , OPENCV ALWAYS CONVERTS TO ROW MAJOR)
    

    /*for(int i =0; i<array.size(); i++)
    {
        cout << array[i] << "\t";
    }*/

    //works fine till here


    Size s = greyMat.size();
    // rows = s.height;
    // cols = s.width;

    //<--------------------------------------CREATING GAUSSIAN FILTER AND CONVERTING IT TO SPARSE MATRIX------------------>
  
    gauss_fil GAUSS((float)1.0,(float)2.0,array.size(),(int)s.width,(int)s.height );
    // for (int i = 0; i < array.size(); ++i)
    // {
    //     cout << array[i] << "\t";
    // }

    //<---------------------------------------CONVERTING spG to CSR format i.e. spG_csr----------------------------------->

    //cout << "\n spG[0][0] : \n" << GAUSS.spG.size();

    //works fine till here

    cout << "\n\n\n\n\n <-------------------- CONVERTING spG to spG_CSR  -------------------> \n\n\n\n\n\n\n";

    CSR spG_csr(GAUSS.spG);
    //works fine till here


    
    // //############################################################# PREVIOUS CUDA CODE 0 ##############################################
    int    *d_Ap,*d_Aj;
    float  /**Av,*/ *x, *y;
    float  *d_Av, *d_x, *d_y; 



    int X,Y,i;
    X = array.size();
    Y = array.size();
    int num_rows= array.size();
    //------------------------ Allocate host memory-------------------
    y    = (float*)malloc(sizeof(float) *     Y);
    
    


    //----------------------- Allocate device memory -----------------
    cudaMalloc((void**)&d_Ap, sizeof(int  ) * spG_csr.P);
    cudaMalloc((void**)&d_Aj, sizeof(int  ) * spG_csr.J);
    cudaMalloc((void**)&d_Av, sizeof(float) * spG_csr.V);
    cudaMalloc((void**)&d_x , sizeof(float) *         X);
    cudaMalloc((void**)&d_y , sizeof(float) *         Y);

    //------------- Transfer data from host to device memory-----------
    cudaMemcpy(d_Ap, &spG_csr.Ap[0], sizeof(int  ) * spG_csr.P, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Aj, &spG_csr.Aj[0], sizeof(int  ) * spG_csr.J, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Av, &spG_csr.Av[0], sizeof(float) * spG_csr.V, cudaMemcpyHostToDevice);
    cudaMemcpy(d_x , &array[0]     , sizeof(float) *         X, cudaMemcpyHostToDevice);

     //--------------------------- Executing kernel ------------------
    cout << "\n\n\n\n\n <--------------------EXECUTING CUDA KERNEL 0 -------------------> \n\n\n\n\n\n\n";
    int block_size = 256;
    int grid_size = ((X + block_size -1) / block_size);
    csrmul_kernel<<<grid_size,block_size>>>(d_Ap, d_Aj,d_Av, num_rows, d_x, d_y);
    //gridsize = no. of blocks per grid and blocksize = no .of threads per block
    
    //----------------- Transfer data back to host memory---------------
    cudaMemcpy(y, d_y, sizeof(float) * Y, cudaMemcpyDeviceToHost);

    

    printf("PASSED\n");

    // Deallocate device memory
    cudaFree(d_Ap);
    cudaFree(d_Av);
    cudaFree(d_Aj);
    cudaFree(d_x);
    cudaFree(d_y);

    // Deallocate host memory
    /*free(&spG_csr.Ap[0]); 
    free(&spG_csr.Av[0]); 
    free(&spG_csr.Aj[0]);
    free(&array[0]);
    free(y);*/

    //############################################################# PREVIOUS CUDA CODE 0 ##############################################
    
    // convert 1d image back to 2d

    cout << "AFTER GAUSSIAN"  << "\n";
    Mat gaussShow = Mat(s.height,s.width,CV_8UC1,y);
    imshow("gauss",gaussShow);


    vector<vector<float>> after_gauss;// dim of ans matrix is dimX * dimY2
    // temp=1;
    for(int k=0; k< (int)greyMat.rows ; k++)
    {
       after_gauss.push_back(std::vector<float>());
       for(int p=0; p< (int)greyMat.cols ; p++)
       {
           //cout << y[p*(int)greyMat.rows + k] << "\t";
           
           after_gauss[k].push_back((int)y[p*(int)greyMat.rows + k]);
           
       }
       cout << "\n";
    }

    
    
    // for(int k=0; k< (int)greyMat.rows ; k++)
    // {
       
    //    for(int p=0; p< (int)greyMat.cols ; p++)
    //    {
           
           
    //        cout << after_gauss[k][p] << "\t";
           
    //    }
    //    cout << "\n";
    // }

     // cv::Mat A(after_gauss.size(), (int)greyMat.cols, CV_8UC1);
     // for(int i=0; i<A.rows; ++i)
     // {
     //     for(int j=0; j<A.cols; ++j)
     //     {
     //        A.at<int>(i, j) = (int)after_gauss[i][j];  
     //     }
     //     cout<<i<<" ";
     // }
          //A(i, j) = (int)after_gauss[i][j];

    // cout << "A = " << endl << " "  << A << endl << endl;


    //<-----------------------------------------------SOBEL FIL X PART -------------------------------------------------------->

    std::vector<float> array2;

    //<================== get a column major out of after gauss ========================>

    for(int k=0; k< (int)greyMat.cols ; k++)
    {
       
       for(int p=0; p< (int)greyMat.rows ; p++)
       {
           
           array2.push_back( after_gauss[k][p] );
           
       }
       cout << "\n";
    }
   
    sobel_filx SOLX(array2.size(),(int)greyMat.cols,(int)greyMat.rows );
    
    cout << "\n\n\n\n\n <-------------------- CONVERTING spGx to spG_CSRx  -------------------> \n\n\n\n\n\n\n";

    CSR spG_csrx(SOLX.spGx);

    //imshow( "Display window", A);

    // //############################################################# PREVIOUS CUDA CODE 1 ##############################################

    int X2,Y2;
    X2 = array2.size();
    Y2 = array2.size();
    int num_rows2= array2.size();
    //------------------------ Allocate host memory-------------------
    float * y2;
    y2    = (float*)malloc(sizeof(float) *     Y2);
    
    


    //----------------------- Allocate device memory -----------------
    cudaMalloc((void**)&d_Ap, sizeof(int  ) * spG_csrx.P);
    cudaMalloc((void**)&d_Aj, sizeof(int  ) * spG_csrx.J);
    cudaMalloc((void**)&d_Av, sizeof(float) * spG_csrx.V);
    cudaMalloc((void**)&d_x , sizeof(float) *         X2);
    cudaMalloc((void**)&d_y , sizeof(float) *         Y2);

    //------------- Transfer data from host to device memory-----------
    cudaMemcpy(d_Ap, &spG_csrx.Ap[0], sizeof(int  ) * spG_csrx.P, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Aj, &spG_csrx.Aj[0], sizeof(int  ) * spG_csrx.J, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Av, &spG_csrx.Av[0], sizeof(float) * spG_csrx.V, cudaMemcpyHostToDevice);
    cudaMemcpy(d_x , &array2[0]     , sizeof(float) *         X2, cudaMemcpyHostToDevice);

     //--------------------------- Executing kernel ------------------
    cout << "\n\n\n\n\n <--------------------EXECUTING CUDA KERNEL 1 -------------------> \n\n\n\n\n\n\n";
    //int block_size = 256;
    int grid_size2 = ((X2 + block_size -1) / block_size);
    csrmul_kernel<<<grid_size2,block_size>>>(d_Ap, d_Aj,d_Av, num_rows2, d_x, d_y);
    //gridsize = no. of blocks per grid and blocksize = no .of threads per block
    
    //----------------- Transfer data back to host memory---------------
    cudaMemcpy(y2, d_y, sizeof(float) * Y2, cudaMemcpyDeviceToHost);

    

    printf("PASSED\n");

    // Deallocate device memory
    cudaFree(d_Ap);
    cudaFree(d_Av);
    cudaFree(d_Aj);
    cudaFree(d_x);
    cudaFree(d_y);

    // Deallocate host memory
    /*free(&spG_csr.Ap[0]); 
    free(&spG_csr.Av[0]); 
    free(&spG_csr.Aj[0]);
    free(&array[0]);
    free(y);*/

    //############################################################# PREVIOUS CUDA CODE 1 ##############################################
    
    // convert 1d image back to 2d (the second time)

    cout << "AFTER SOBEL FIL X"  << "\n";
    Mat sobelxShow = Mat(s.height,s.width,CV_8UC1,y2);
    imshow("sobx",sobelxShow);

    vector<vector<float>> after_sobelx2;// dim of ans matrix is dimX * dimY2
    // temp=1;
    for(int k=0; k< (int)greyMat.rows ; k++)
    {
       after_sobelx2.push_back(std::vector<float>());
       for(int p=0; p< (int)greyMat.cols ; p++)
       {
           //cout << y[p*(int)greyMat.rows + k] << "\t";
           
           after_sobelx2[k].push_back((int)y2[p*(int)greyMat.rows + k]);
           
       }
       cout << "\n";
    }

    //<-------------------------------------------------sobel filter Y part ---------------------------------------------------------

    sobel_fily SOLY(array2.size(),(int)greyMat.cols,(int)greyMat.rows );
    
    cout << "\n\n\n\n\n <-------------------- CONVERTING spGy to spG_csry  -------------------> \n\n\n\n\n\n\n";

    CSR spG_csry(SOLY.spGy);

    // //############################################################# PREVIOUS CUDA CODE 2 ##############################################

    int X3,Y3;
    X2 = array2.size();
    Y2 = array2.size();
    //int num_rows2= array2.size();
    //------------------------ Allocate host memory-------------------
    float * y3;
    y3    = (float*)malloc(sizeof(float) *     Y2);
    
    


    //----------------------- Allocate device memory -----------------
    cudaMalloc((void**)&d_Ap, sizeof(int  ) * spG_csry.P);
    cudaMalloc((void**)&d_Aj, sizeof(int  ) * spG_csry.J);
    cudaMalloc((void**)&d_Av, sizeof(float) * spG_csry.V);
    cudaMalloc((void**)&d_x , sizeof(float) *         X2);
    cudaMalloc((void**)&d_y , sizeof(float) *         Y2);

    //------------- Transfer data from host to device memory-----------
    cudaMemcpy(d_Ap, &spG_csry.Ap[0], sizeof(int  ) * spG_csry.P, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Aj, &spG_csry.Aj[0], sizeof(int  ) * spG_csry.J, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Av, &spG_csry.Av[0], sizeof(float) * spG_csry.V, cudaMemcpyHostToDevice);
    cudaMemcpy(d_x , &array2[0]     , sizeof(float) *         X2, cudaMemcpyHostToDevice);

     //--------------------------- Executing kernel ------------------
    cout << "\n\n\n\n\n <--------------------EXECUTING CUDA KERNEL 2 -------------------> \n\n\n\n\n\n\n";
    //int block_size = 256;
    //int grid_size2 = ((X2 + block_size -1) / block_size);
    csrmul_kernel<<<grid_size2,block_size>>>(d_Ap, d_Aj,d_Av, num_rows2, d_x, d_y);
    //gridsize = no. of blocks per grid and blocksize = no .of threads per block
    
    //----------------- Transfer data back to host memory---------------
    cudaMemcpy(y3, d_y, sizeof(float) * Y2, cudaMemcpyDeviceToHost);

    

    printf("PASSED\n");

    // Deallocate device memory
    cudaFree(d_Ap);
    cudaFree(d_Av);
    cudaFree(d_Aj);
    cudaFree(d_x);
    cudaFree(d_y);

    // Deallocate host memory
    /*free(&spG_csr.Ap[0]); 
    free(&spG_csr.Av[0]); 
    free(&spG_csr.Aj[0]);
    free(&array[0]);
    free(y);*/

    //############################################################# PREVIOUS CUDA CODE 2 ##############################################
    
    // convert 1d image back to 2d (the second time)

    cout << "AFTER SOBELY"  << "\n";
    Mat sobyShow = Mat(s.height,s.width,CV_8UC1,y3);
    imshow("soby",sobyShow);


    vector<vector<float>> after_sobely2;// dim of ans matrix is dimX * dimY2
    // temp=1;
    for(int k=0; k< (int)greyMat.rows ; k++)
    {
       after_sobely2.push_back(std::vector<float>());
       for(int p=0; p< (int)greyMat.cols ; p++)
       {
           //cout << y[p*(int)greyMat.rows + k] << "\t";
           
           after_sobely2[k].push_back((int)y3[p*(int)greyMat.rows + k]);
           
       }
       cout << "\n";
    }

    //****************************************************CALCULATE ARCTAN / GRADIENT USING CUDA **********************************

    //------------------------ Allocate host memory-------------------
    int Y4 = Y2;
    float * yatan;
    yatan    = (float*)malloc(sizeof(float) *     Y4);

    //----------------------- Allocate device memory -----------------
    float * d_y2,* d_y3;
    cudaMalloc((void**)&d_y2, sizeof(float) *        Y2);
    cudaMalloc((void**)&d_y3, sizeof(float) *        Y2);
    cudaMalloc((void**)&d_y , sizeof(float) *        Y4);

    //------------- Transfer data from host to device memory-----------
    cudaMemcpy(d_y2 , &y2[0]     , sizeof(float) * Y2, cudaMemcpyHostToDevice);
    cudaMemcpy(d_y3 , &y3[0]     , sizeof(float) * Y2, cudaMemcpyHostToDevice);

    //--------------------------- Executing kernel ------------------
    cout << "\n\n\n\n\n <--------------------EXECUTING CUDA KERNEL 3 -------------------> \n\n\n\n\n\n\n";
    //int block_size = 256;
    //int grid_size2 = ((X2 + block_size -1) / block_size);
    arctan_kernel<<<grid_size2,block_size>>>(d_y2, d_y3 ,Y2, d_y);

    //----------------- Transfer data back to host memory---------------
    cudaMemcpy(yatan, d_y, sizeof(float) * Y4, cudaMemcpyDeviceToHost);

    cudaFree(d_y2);
    cudaFree(d_y3);
    cudaFree(d_y);

    // for (int i = 0; i < Y4; ++i)
    // {
    //      cout << yatan[i] << "\t";
    // }

    //****************************************************CALCULATE MAGNITUDE OF GRADIENT  **********************************
    
    //------------------------ Allocate host memory-------------------
    int Y5 = Y2;
    float * ymag;
    ymag = (float*)malloc(sizeof(float) *     Y5);

    //----------------------- Allocate device memory -----------------
    cudaMalloc((void**)&d_y2, sizeof(float) *        Y2);
    cudaMalloc((void**)&d_y3, sizeof(float) *        Y2);
    cudaMalloc((void**)&d_y , sizeof(float) *        Y5);
    

    //------------- Transfer data from host to device memory-----------
    cudaMemcpy(d_y2 , &y2[0]     , sizeof(float) * Y2, cudaMemcpyHostToDevice);
    cudaMemcpy(d_y3 , &y3[0]     , sizeof(float) * Y2, cudaMemcpyHostToDevice);

    //--------------------------- Executing kernel ------------------
    cout << "\n\n\n\n\n <--------------------EXECUTING CUDA KERNEL 4 -------------------> \n\n\n\n\n\n\n";
    //int block_sizcout << "AFTER GAUSSIAN"  << "\n";
//     Mat gaussShow = Mat(s.height,s.width,CV_8UC1,y);
//     imshow("gauss",gaussShow);
// e = 256;
    //int grid_size2 = ((X2 + block_size -1) / block_size);
    mag_kernel<<<grid_size2,block_size>>>(d_y2, d_y3 ,Y2, d_y);

    //----------------- Transfer data back to host memory---------------
    cudaMemcpy(ymag, d_y, sizeof(float) * Y4, cudaMemcpyDeviceToHost);

    cudaFree(d_y2);
    cudaFree(d_y3);
    cudaFree(d_y);

    // for (int i = 0; i < Y4; ++i)
    // {
    //      cout << ymag[i] << "\t";
    // }

    //**************************************************** NON MAXIMUM SUPRESSION  **********************************
    // required:-
    // 1) gradients
    // 2) magnitudes
    
    //------------------------ Allocate host memory-------------------
    
    float * yNMS;
    yNMS = (float*)malloc(sizeof(float) *     Y2);

    //----------------------- Allocate device memory -----------------
    cudaMalloc((void**)&d_y2, sizeof(float) *        Y2);
    cudaMalloc((void**)&d_y3, sizeof(float) *        Y2);
    cudaMalloc((void**)&d_y , sizeof(float) *        Y2);
    
    //------------- Transfer data from host to device memory-----------
    cudaMemcpy(d_y2 , &yatan[0]     , sizeof(float) * Y2, cudaMemcpyHostToDevice);
    cudaMemcpy(d_y3 , &ymag[0]      , sizeof(float) * Y2, cudaMemcpyHostToDevice);

    //--------------------------- Executing kernel ------------------
    // cout << "\n\n\n\n\n <--------------------EXECUTING CUDA KERNEL 5 -------------------> \n\n\n\n\n\n\n";
    // //int block_size = 256;
    // //int grid_size2 = ((X2 + block_size -1) / block_size);

    // dim3 gridXY;
    // gridXY.x = (int)greyMat.rows/32, gridXY.y = (int)greyMat.cols/32;

    // dim3 dimBlock(32, 32);
    // //dim3 dimGrid(, (int)greyMat.cols/32);
    // NMS_kernel<<<gridXY,dimBlock>>>(d_y2, d_y3 ,(int)greyMat.rows,(int)greyMat.cols, d_y,  M_PI  );

    // //(d_y2, d_y3 ,Y2, d_y,  M_PI, (int)greyMat.cols ,(int)greyMat.rows);


    // //----------------- Transfer data back to host memory---------------
    // cudaMemcpy(yNMS, d_y, sizeof(float) * Y2, cudaMemcpyDeviceToHost);

    // cudaFree(d_y2);
    // cudaFree(d_y3);
    // cudaFree(d_y);

    

    cout << "\n works here\n";

    waitKey(0);                                          // Wait for a keystroke in the window
    return 0;
}