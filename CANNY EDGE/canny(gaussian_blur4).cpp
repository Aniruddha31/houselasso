#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <string>
#include <opencv2/imgproc/imgproc.hpp>
#include <cuda_runtime.h>
#include "2d_to_row_major.h"
#include "gauss_fil.h"
#include "csr_matrix.h"
#include "sobel_filx.h"
#include "sobel_fily.h"
#include <cuda_fp16.h>// for __shfl_down_sync() function
                       //       1)permit exchanging of a variable between threads within a warp without use of shared memory


using namespace cv;
using namespace std;

//<---------------------------------DEFINE SOBEL FILTERS-------------------------------->//
std::vector<vector<float>> GXfilter {{-1,0,1},{-2,0,2},{-1,0,1} }; // gradient in X => edge in Y
std::vector<vector<float>> GYfilter {{-1,-2,-1},{0,0,0},{1,2,1} }; // gradient in Y => edge in X



//############################################################## PREVIOUS CUDA CODE ###############################################>

//------------------------- CONSTANT HEADERS------------------------------//
#define WARP_MASK 0xffffffff

//------------------------------------------------------------------------//


// template <class T>
// __device__ T warp_reduce (T val)
// {
//    for(int offset = warpSize/2; offset>0 ; offset=offset/2)
//        val = val + __shfl_down_sync(WARP_MASK, val, offset);

//    return val;
// }

// __global__ void csrmul_kernel(int *Ap, int *Aj,float *Av, int num_rows, float *x, float *y)
// {
//    int thread_id = blockIdx.x*blockDim.x + threadIdx.x;
//    int warp_id = thread_id / 32;
//    int lane = thread_id % 32;
//    int row = warp_id; // <<-----one warp per row !!!------>>

//    float sum = 0;

//    if( row<num_rows )
//    {
//       int row_begin = Ap[row];
//       int row_end = Ap[row+1];

//       //y[row] = multiply_row(row_end-row_begin, Aj+row_begin,Av+row_begin, x); 
//       for(int ele = row_begin + lane; ele < row_end; ele = ele + 32)
//           sum = sum + Av[ele] * x[Aj[ele]];
//    }
//    sum = warp_reduce (sum);
   
//    if( lane == 0 && row < num_rows)
//       y[row] = sum;
// }

__device__ float multiply_row(int rowsize, int *Aj, float *Av,float *x)
{
   float sum = 0;
   for( int column=0; column<rowsize; ++column)
   sum += Av[column] * x[Aj[column]];
   return sum;
}

__global__ void csrmul_kernel(int *Ap, int *Aj,float *Av, int num_rows, float *x, float *y)
{
   int row = blockIdx.x*blockDim.x + threadIdx.x;
   if( row<num_rows )
   {
      int row_begin = Ap[row];
      int row_end = Ap[row+1];

      y[row] = multiply_row(row_end-row_begin, Aj+row_begin,Av+row_begin, x);
   }
}

//############################################################## PREVIOUS CUDA CODE ###############################################>




int main( )
{

    Mat image;
    image = imread("index.jpeg", CV_LOAD_IMAGE_COLOR);   // Read the file

    cv::Mat greyMat,greyMatpadded;
    cv::cvtColor(image, greyMat, cv::COLOR_BGR2GRAY);
    greyMat.convertTo(greyMat,CV_32F);

    transpose(greyMat, greyMat);  // USE SINCE my algo works for column major and it will output row major after execution

    // Initialize arguments for PADDING
    int PAD =2;
    int top = (int) (greyMat.rows + PAD); 
    int bottom = top;
    int left = (int) (greyMat.cols + PAD); 
    int right = left;

    cout << "\n greyMat.cols): " << greyMat.cols << "\n";

    //copyMakeBorder( greyMat, greyMatpadded, top, bottom, left, right, BORDER_CONSTANT, 255 );

    //imshow( "Display window", greyMat );

    // convert to float vector
    std::vector<float> array((float*)greyMat.data, (float*)greyMat.data + greyMat.rows * greyMat.cols);

    

    /*for(int i =0; i<array.size(); i++)
    {
        cout << array[i] << "\t";
    }*/

    //works fine till here

    //<--------------------------------------CREATING GAUSSIAN FILTER AND CONVERTING IT TO SPARSE MATRIX------------------>
  
    gauss_fil GAUSS((float)1.0,(float)2.0,array.size(),(int)greyMat.cols,(int)greyMat.rows );
    // for (int i = 0; i < array.size(); ++i)
    // {
    //     cout << array[i] << "\t";
    // }

    //<---------------------------------------CONVERTING spG to CSR format i.e. spG_csr----------------------------------->

    //cout << "\n spG[0][0] : \n" << GAUSS.spG.size();

    //works fine till here

    cout << "\n\n\n\n\n <-------------------- CONVERTING spG to spG_CSR  -------------------> \n\n\n\n\n\n\n";

    CSR spG_csr(GAUSS.spG);
    //works fine till here


    
    // //############################################################# PREVIOUS CUDA CODE 0 ##############################################
    int    *d_Ap,*d_Aj;
    float  /**Av,*/ *x, *y;
    float  *d_Av, *d_x, *d_y; 



    int X,Y,i;
    X = array.size();
    Y = array.size();
    int num_rows= array.size();
    //------------------------ Allocate host memory-------------------
    y    = (float*)malloc(sizeof(float) *     Y);
    
    


    //----------------------- Allocate device memory -----------------
    cudaMalloc((void**)&d_Ap, sizeof(int  ) * spG_csr.P);
    cudaMalloc((void**)&d_Aj, sizeof(int  ) * spG_csr.J);
    cudaMalloc((void**)&d_Av, sizeof(float) * spG_csr.V);
    cudaMalloc((void**)&d_x , sizeof(float) *         X);
    cudaMalloc((void**)&d_y , sizeof(float) *         Y);

    //------------- Transfer data from host to device memory-----------
    cudaMemcpy(d_Ap, &spG_csr.Ap[0], sizeof(int  ) * spG_csr.P, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Aj, &spG_csr.Aj[0], sizeof(int  ) * spG_csr.J, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Av, &spG_csr.Av[0], sizeof(float) * spG_csr.V, cudaMemcpyHostToDevice);
    cudaMemcpy(d_x , &array[0]     , sizeof(float) *         X, cudaMemcpyHostToDevice);

     //--------------------------- Executing kernel ------------------
    cout << "\n\n\n\n\n <--------------------EXECUTING CUDA KERNEL -------------------> \n\n\n\n\n\n\n";
    int block_size = 256;
    int grid_size = ((X + block_size -1) / block_size);
    csrmul_kernel<<<grid_size,block_size>>>(d_Ap, d_Aj,d_Av, num_rows, d_x, d_y);
    //gridsize = no. of blocks per grid and blocksize = no .of threads per block
    
    //----------------- Transfer data back to host memory---------------
    cudaMemcpy(y, d_y, sizeof(float) * Y, cudaMemcpyDeviceToHost);

    

    printf("PASSED\n");

    // Deallocate device memory
    cudaFree(d_Ap);
    cudaFree(d_Av);
    cudaFree(d_Aj);
    cudaFree(d_x);
    cudaFree(d_y);

    // Deallocate host memory
    /*free(&spG_csr.Ap[0]); 
    free(&spG_csr.Av[0]); 
    free(&spG_csr.Aj[0]);
    free(&array[0]);
    free(y);*/

    //############################################################# PREVIOUS CUDA CODE 0 ##############################################
    
    // convert 1d image back to 2d

    vector<vector<float>> after_gauss;// dim of ans matrix is dimX * dimY2
    // temp=1;
    for(int k=0; k< (int)greyMat.rows ; k++)
    {
       after_gauss.push_back(std::vector<float>());
       for(int p=0; p< (int)greyMat.cols ; p++)
       {
           //cout << y[p*(int)greyMat.rows + k] << "\t";
           
           after_gauss[k].push_back((int)y[p*(int)greyMat.rows + k]);
           
       }
       cout << "\n";
    }

    
    
    // for(int k=0; k< (int)greyMat.rows ; k++)
    // {
       
    //    for(int p=0; p< (int)greyMat.cols ; p++)
    //    {
           
           
    //        cout << after_gauss[k][p] << "\t";
           
    //    }
    //    cout << "\n";
    // }

     // cv::Mat A(after_gauss.size(), (int)greyMat.cols, CV_8UC1);
     // for(int i=0; i<A.rows; ++i)
     // {
     //     for(int j=0; j<A.cols; ++j)
     //     {
     //        A.at<int>(i, j) = (int)after_gauss[i][j];  
     //     }
     //     cout<<i<<" ";
     // }
          //A(i, j) = (int)after_gauss[i][j];

    // cout << "A = " << endl << " "  << A << endl << endl;


    //<-----------------------------------------------SOBEL FIL X PART -------------------------------------------------------->

    std::vector<float> array2;

    //<================== get a column major out of after gauss ========================>

    for(int k=0; k< (int)greyMat.cols ; k++)
    {
       
       for(int p=0; p< (int)greyMat.rows ; p++)
       {
           
           array2.push_back( after_gauss[k][p] );
           
       }
       cout << "\n";
    }
   
    sobel_filx SOLX(array2.size(),(int)greyMat.cols,(int)greyMat.rows );
    
    cout << "\n\n\n\n\n <-------------------- CONVERTING spGx to spG_CSRx  -------------------> \n\n\n\n\n\n\n";

    CSR spG_csrx(SOLX.spGx);

    //imshow( "Display window", A);

    // //############################################################# PREVIOUS CUDA CODE 1 ##############################################

    int X2,Y2;
    X2 = array2.size();
    Y2 = array2.size();
    int num_rows2= array2.size();
    //------------------------ Allocate host memory-------------------
    float * y2;
    y2    = (float*)malloc(sizeof(float) *     Y2);
    
    


    //----------------------- Allocate device memory -----------------
    cudaMalloc((void**)&d_Ap, sizeof(int  ) * spG_csrx.P);
    cudaMalloc((void**)&d_Aj, sizeof(int  ) * spG_csrx.J);
    cudaMalloc((void**)&d_Av, sizeof(float) * spG_csrx.V);
    cudaMalloc((void**)&d_x , sizeof(float) *         X2);
    cudaMalloc((void**)&d_y , sizeof(float) *         Y2);

    //------------- Transfer data from host to device memory-----------
    cudaMemcpy(d_Ap, &spG_csrx.Ap[0], sizeof(int  ) * spG_csrx.P, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Aj, &spG_csrx.Aj[0], sizeof(int  ) * spG_csrx.J, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Av, &spG_csrx.Av[0], sizeof(float) * spG_csrx.V, cudaMemcpyHostToDevice);
    cudaMemcpy(d_x , &array2[0]     , sizeof(float) *         X2, cudaMemcpyHostToDevice);

     //--------------------------- Executing kernel ------------------
    cout << "\n\n\n\n\n <--------------------EXECUTING CUDA KERNEL -------------------> \n\n\n\n\n\n\n";
    //int block_size = 256;
    int grid_size2 = ((X2 + block_size -1) / block_size);
    csrmul_kernel<<<grid_size2,block_size>>>(d_Ap, d_Aj,d_Av, num_rows2, d_x, d_y);
    //gridsize = no. of blocks per grid and blocksize = no .of threads per block
    
    //----------------- Transfer data back to host memory---------------
    cudaMemcpy(y2, d_y, sizeof(float) * Y2, cudaMemcpyDeviceToHost);

    

    printf("PASSED\n");

    // Deallocate device memory
    cudaFree(d_Ap);
    cudaFree(d_Av);
    cudaFree(d_Aj);
    cudaFree(d_x);
    cudaFree(d_y);

    // Deallocate host memory
    /*free(&spG_csr.Ap[0]); 
    free(&spG_csr.Av[0]); 
    free(&spG_csr.Aj[0]);
    free(&array[0]);
    free(y);*/

    //############################################################# PREVIOUS CUDA CODE 1 ##############################################
    
    // convert 1d image back to 2d (the second time)

    vector<vector<float>> after_sobelx2;// dim of ans matrix is dimX * dimY2
    // temp=1;
    for(int k=0; k< (int)greyMat.rows ; k++)
    {
       after_sobelx2.push_back(std::vector<float>());
       for(int p=0; p< (int)greyMat.cols ; p++)
       {
           //cout << y[p*(int)greyMat.rows + k] << "\t";
           
           after_sobelx2[k].push_back((int)y2[p*(int)greyMat.rows + k]);
           
       }
       cout << "\n";
    }

    //<-------------------------------------------------sobel filter Y part ---------------------------------------------------------

    sobel_fily SOLY(array2.size(),(int)greyMat.cols,(int)greyMat.rows );
    
    cout << "\n\n\n\n\n <-------------------- CONVERTING spGy to spG_csry  -------------------> \n\n\n\n\n\n\n";

    CSR spG_csry(SOLY.spGy);

    // //############################################################# PREVIOUS CUDA CODE 2 ##############################################

    int X3,Y3;
    X2 = array2.size();
    Y2 = array2.size();
    //int num_rows2= array2.size();
    //------------------------ Allocate host memory-------------------
    float * y3;
    y3    = (float*)malloc(sizeof(float) *     Y2);
    
    


    //----------------------- Allocate device memory -----------------
    cudaMalloc((void**)&d_Ap, sizeof(int  ) * spG_csry.P);
    cudaMalloc((void**)&d_Aj, sizeof(int  ) * spG_csry.J);
    cudaMalloc((void**)&d_Av, sizeof(float) * spG_csry.V);
    cudaMalloc((void**)&d_x , sizeof(float) *         X2);
    cudaMalloc((void**)&d_y , sizeof(float) *         Y2);

    //------------- Transfer data from host to device memory-----------
    cudaMemcpy(d_Ap, &spG_csry.Ap[0], sizeof(int  ) * spG_csry.P, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Aj, &spG_csry.Aj[0], sizeof(int  ) * spG_csry.J, cudaMemcpyHostToDevice);
    cudaMemcpy(d_Av, &spG_csry.Av[0], sizeof(float) * spG_csry.V, cudaMemcpyHostToDevice);
    cudaMemcpy(d_x , &array2[0]     , sizeof(float) *         X2, cudaMemcpyHostToDevice);

     //--------------------------- Executing kernel ------------------
    cout << "\n\n\n\n\n <--------------------EXECUTING CUDA KERNEL -------------------> \n\n\n\n\n\n\n";
    //int block_size = 256;
    //int grid_size2 = ((X2 + block_size -1) / block_size);
    csrmul_kernel<<<grid_size2,block_size>>>(d_Ap, d_Aj,d_Av, num_rows2, d_x, d_y);
    //gridsize = no. of blocks per grid and blocksize = no .of threads per block
    
    //----------------- Transfer data back to host memory---------------
    cudaMemcpy(y3, d_y, sizeof(float) * Y2, cudaMemcpyDeviceToHost);

    

    printf("PASSED\n");

    // Deallocate device memory
    cudaFree(d_Ap);
    cudaFree(d_Av);
    cudaFree(d_Aj);
    cudaFree(d_x);
    cudaFree(d_y);

    // Deallocate host memory
    /*free(&spG_csr.Ap[0]); 
    free(&spG_csr.Av[0]); 
    free(&spG_csr.Aj[0]);
    free(&array[0]);
    free(y);*/

    //############################################################# PREVIOUS CUDA CODE 2 ##############################################
    
    // convert 1d image back to 2d (the second time)

    vector<vector<float>> after_sobely2;// dim of ans matrix is dimX * dimY2
    // temp=1;
    for(int k=0; k< (int)greyMat.rows ; k++)
    {
       after_sobely2.push_back(std::vector<float>());
       for(int p=0; p< (int)greyMat.cols ; p++)
       {
           //cout << y[p*(int)greyMat.rows + k] << "\t";
           
           after_sobely2[k].push_back((int)y3[p*(int)greyMat.rows + k]);
           
       }
       cout << "\n";
    }

    //****************************************************CALCULATE ARCTAN / GRADIENT USING CUDA **********************************

    //------------------------ Allocate host memory-------------------
    int Y3 = y3.size();
    float * yatan;
    yatan    = (float*)malloc(sizeof(float) *     Y3);

    
    


    cout << "\n works here\n";

    waitKey(0);                                          // Wait for a keystroke in the window
    return 0;
}