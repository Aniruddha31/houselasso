#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <vector>
#include <math.h>
#include <cuda.h>
#include <cuda_runtime.h>
using namespace std;

class row_major
{
public:
	// construct Ap, Aj, Av
	vector<float> rmmat;//row major mat
        int d_X;
        int d_Y;

	// row_major Constructor
	row_major(vector<vector<float>> const &rows)
	{
		// 
		d_X=rows.size(); // size of outer vector
                cout << "\nd_X is:" << d_X;

                d_Y=rows[0].size();
                cout << "\nd_Y is:" << d_Y;
               

                //---------LOOP MATRIX create Av and Aj-----------
        
                for(int i=0; i<d_X;i++  )
                {
                    for(int j=0; j<d_Y; j++) //iterate full row "i" [i,j]
                    {

                        rmmat.push_back(rows[i][j]);
                    }
                    
                }
          
                //------------END LOOP-----------------------------
 

                //-------------CHECK VALUES------------------------
                cout << "\nrmmat is:\n";
                for(int i=0; i< rmmat.size();i++)
                     cout << rmmat[i] << "\t";

                
                cout << "\n";
	}
};




