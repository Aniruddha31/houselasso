#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <vector>
#include <math.h>
//# define M_PI           3.14159265358979323846  /* pi*/
using namespace std;

class gauss_fil
{
public:
	// construct G
    vector<vector<float>> spG;
    vector<vector<float>> G;
    float sigma ;
    float  s ;

	// gauss_fil Constructor
	gauss_fil(float val1,float val2, int img_flat_size, int img_width , int img_height): spG(img_flat_size, std::vector <float> (img_flat_size, 0.0))

	{
		// 
		sigma = val1;
        cout << "\nsigma is:\n" << sigma;

        s = val2 * sigma * sigma;
        cout << "\ns is:\n" << s;

        float sum = 0.0;
        float r;

        //---------LOOP MATRIX create Av and Aj-----------
        
        // generating 5x5 kernel 

        for (int x = -2; x <= 2; x++) {                     // 0 4
            //G.push_back(std::vector<float>());
            for (int y = -2; y <= 2; y++) { 
                r = sqrt(x * x + y * y); 
                //G[x].push_back(    (   exp(-(r * r) / s)   ) / (M_PI * s)     ); 
                sum += (   exp(-(r * r) / s)   ) / (M_PI * s);//G[x + 2][y + 2]; 
            } 
        } 

        

       
         //------------------normalize matrix ---------------
        int i,j;
        i=0;
        for (int x = -2; x <= 2; x++) { 
            
            G.push_back(vector<float>());
            for (int y = -2; y <= 2; y++) { 
                r = sqrt(x * x + y * y);
                G[i].push_back(    ((   exp(-(r * r) / s)   ) / (M_PI * s))/sum    ); 
                
            } 
            i = i+1;
        }
          
        //------------END LOOP-----------------------------

       
        
        
        //----------------CHECK VALUES------------------//
        cout << "\n";
        for (int x = 0; x < G.size(); x++) { 
            
            for (int y = 0; y < 5/*G[x].size()*/; y++) {   
                cout << G[x][y] << "\t";  
            } 
            cout << "\n";
        }  
        cout << "\n G.size is :" << G.size() << "\n";
        cout << "\n G[0].size is :" << G[0].size() << "\n";

        // works fine till here    

        //<###################################################################################################################3

        /*// hard code for 5x5 gaussian filter
        int h=0;
        spG.push_back(std::vector<float>());
        h=1;
        spG.push_back(std::vector<float>());
        spG[h].push_back(0.0);
        h=h+1;
        spG.push_back(std::vector<float>());
        spG[h].push_back(0.0);spG[h].push_back(0.0);
        h=h+1;
        spG.push_back(std::vector<float>());
        spG[h].push_back(0.0);spG[h].push_back(0.0);spG[h].push_back(0.0);
        h=h+1;
        spG.push_back(std::vector<float>());
        spG[h].push_back(0.0);spG[h].push_back(0.0);spG[h].push_back(0.0);spG[h].push_back(0.0);
        //<------------------------------------------------------------------------------------------>

        
            //spG.push_back(vector<float>());

            // strategy :-
            // i need a var to go till G.size()
            // i need a var to go till 

        cout << "\nimg_width is: " << img_width << "\n";
        cout << "\nimg_flat_size is: " << img_flat_size << "\n";

        int flag=1,k=0,l,m=0;
        j=1;


            /*while(   j<= img_flat_size   )
            {


                
                if(flag == 1)
                {

                    for(k=0;    k< G[0].size();    k++,j++)
                    {
                        // j becomes j+ 4 after this loop ends and j E [0,img_flat_size-1] SEE "FOR LOOP"
                        // loop runs for k = 0,1,2,3,4 i.e 5 values

                        spG[0].push_back(G[m][k]);
                        spG[1].push_back(G[m][k]);
                        spG[2].push_back(G[m][k]);
                        spG[3].push_back(G[m][k]);
                        spG[4].push_back(G[m][k]);

                        if(m < 5) // range of m E [0,4]   :- though i dont need this if statement
                        {
                            m++;
                        }

                        
                    }

                }



                //else
                //{
                    for(l=0;    l+k < img_width ;   l++, j++)
                    {
                        spG[0].push_back((float)0.0);
                        spG[1].push_back((float)0.0);
                        spG[2].push_back((float)0.0);
                        spG[3].push_back((float)0.0);
                        spG[4].push_back((float)0.0);

                    }

                    

                //}
                
            }//  END OF OUTER j loop*/

        /*while(   j<= img_flat_size   )
        {
            if(flag == 1)
            {
                spG.push_back(std::vector<float>());
                spG[0].push_back(G[0][k]);  spG[0].push_back(G[1][k]);   spG[0].push_back(G[2][k]);  spG[0].push_back(G[3][k]);   spG[0].push_back(G[4][k]);
                
                spG.push_back(std::vector<float>());
                spG[1].push_back(G[0][k]);  spG[1].push_back(G[1][k]);   spG[1].push_back(G[2][k]);  spG[1].push_back(G[3][k]);   spG[1].push_back(G[4][k]);
                
                spG.push_back(std::vector<float>());
                spG[2].push_back(G[0][k]);  spG[2].push_back(G[1][k]);   spG[2].push_back(G[2][k]);  spG[2].push_back(G[3][k]);   spG[2].push_back(G[4][k]);
                
                spG.push_back(std::vector<float>());
                spG[3].push_back(G[0][k]);  spG[3].push_back(G[1][k]);   spG[3].push_back(G[2][k]);  spG[3].push_back(G[3][k]);   spG[3].push_back(G[4][k]);
                
                spG.push_back(std::vector<float>());
                spG[4].push_back(G[0][k]);  spG[4].push_back(G[1][k]);   spG[4].push_back(G[2][k]);  spG[4].push_back(G[3][k]);   spG[4].push_back(G[4][k]);

                j= j+4;
            }
        }*/

        //<################################################################################################################################3

        cout << "\nimg_width  is :"  << img_width << "\n";
        cout << "\nimg_height  is :"  << img_height << "\n";
        cout << "\nimg_flat_size  is :"  << img_flat_size << "\n"; // img_flat_size = img_width * img_height is verified 
        
        int A_NUMBER = img_flat_size; //nummber of rows
        //int offset = img_flat_size;
        int OTHER_NUMBER = img_flat_size ;//number of columns
        int DEFAULT_VALUE = 0.0;

        //std::vector <float> line(OTHER_NUMBER, DEFAULT_VALUE);
        /*std::vector<std::vector<float> > spG(A_NUMBER, std::vector <float> (OTHER_NUMBER, DEFAULT_VALUE));*/

        j=0;int flag=1;

        int m=0,n;
        int counter=0;

        while(   j< img_flat_size  && m!=5  )
        {
            cout << "hi one";
            if(flag == 1)
            {
                
                //spG[0][j]= (float)G[0][2];  //spG[0].push_back(G[1][k]);   spG[0].push_back(G[2][k]);  spG[0].push_back(G[3][k]);   spG[0].push_back(G[4][k]);
                for(int k=0;    k< G[0].size();    k++)
                    {
                        // j becomes j+ 4 after this loop ends and j E [0,img_flat_size-1] SEE "FOR LOOP"
                        // loop runs for k = 0,1,2,3,4 i.e 5 values

                        /*spG[0][j]   =(float)G[m][k];
                        spG[1][j+1] =(float)G[m][k];
                        spG[2][j+2] =(float)G[m][k];
                        spG[3][j+3] =(float)G[m][k];
                        spG[4][j+4] =(float)G[m][k];*/

                        for(int n=0; n < img_flat_size /*G[0].size()*G[0].size()*/ ; n++) // img_flat_size here signifies 
                        {
                            if((j+n) < img_flat_size)
                            {
                                //cout << "\n";
                                //cout   << G[m][k] << "\t";
                                spG[n][j+n] =(float)G[m][k];

                                //cout << spG[n][j+n];
                                //cout << "\n";
                            }
                            
                        }

                        //if(m < 5) // range of m E [0,4]   :- though i dont need this if statement
                        //{
                            
                            j++;
                            cout << "\nenjoy j,m:" << j << "\t" << m << "\n";
                        //}

                        
                    }

                    m++;

            }
            cout << "\nj is:" << j << "\n";
            j = j + img_width - G[0].size();
            cout << "\nj is:" << j << "\n";
        }

        //<-------------------------------------------CHECKING VALUES OF spG------------------------------------>
        cout << "\nwidth of spG:" << spG[0].size() << "\n";
        cout << "\nheight of spG:" << spG.size() << "\n";

        cout << "\nspG[1][1] , G[0][0] is :" << spG[img_flat_size-2][img_flat_size-1] << "\t"<< G[0][1] << "\n";

        
        
	}
};